import codecs

#---	class ParserDebug.py
class ParserDebug:
	def __init__(self, debugFile):
		self.dFileName = debugFile
		self.dFileHandle = codecs.open(self.dFileName, 'w', encoding='utf8')

	def writeDebugCALL(self, data):    
		line = "\n*---\nTotal CALL (deleted)  " + str(data.CALLtotal)		
		line += '\n*---'		


		self.dFileHandle.write(line)
		for i in range(data.CALLtotal):
			line = '\n[id] ' + data.CALLid[i]
			line += '\n\t[app] ' + data.CALLapplication[i] 
			line += '\n\t[partner phone FROM] ' + data.CALLfromPartnerPhone[i]
			line += '\n\t[partner identifier FROM] ' + data.CALLfromPartnerIdentifier[i]
			line += '\n\t[partner name FROM] ' + data.CALLfromPartnerName[i]
			line += '\n\t[partner phone TO] ' + data.CALLtoPartnerPhone[i]
			line += '\n\t[partner identifier TO] ' + data.CALLtoPartnerIdentifier[i]
			line += '\n\t[partner name TO] ' + data.CALLtoPartnerName[i]
			line += '\n\t[direction] ' + data.CALLdirection[i]
			line += '\n\t[time] ' + data.CALLtimeStamp[i]
			line += '\n\t[duration]' + data.CALLduration[i] 				
			self.dFileHandle.write(line)

	def writeDebugLOCATION(self, data):    
		line = "\n*---\nTotal LOCATION GPS  " + str(data.LOCATIONtotal)		
		line += '\n*---'		

		self.dFileHandle.write(line)
		for i in range(data.LOCATIONtotal):
			line = '\n[id] ' + data.LOCATIONid[i]
			line += '\n\t[latitude] ' + data.LOCATIONlatitude[i]
			line += '\n\t[longitude] ' + data.LOCATIONlongitude[i]
			line += '\n\t[altitude] ' + data.LOCATIONaltitude[i]
			line += '\n\t[time] ' + data.LOCATIONtimeStamp[i]
			self.dFileHandle.write(line)

	def writeDebugCHAT(self, data):    

		line = "\n*---\nTotal CHAT " + str(data.CHATtotal)
		line += '\n*---'		
		self.dFileHandle.write(line)
		for i in range(data.CHATtotal):
			line = '\n* [CHATid] ' + data.CHATid[i]
			line += '\n\t[application] ' + data.CHATapplication[i]
			line += '\n\t[date_time] ' + data.CHATdateTime[i]
			line += '\n\t[direction] ' + data.CHATdirection[i]
			line += '\n\t[status] ' + data.CHATstatus[i]
			line += '\n\t[type] ' + data.CHATmessageType[i]
			line += '\n\t[thread_id] ' + data.CHATthreadId[i]
			line += '\n\t[body] ' + data.CHATbody[i]
			line += '\n\t[FROM id] ' + data.CHATfromIdentifier[i]
			line += '\n\t[FROM name] ' + data.CHATfromName[i]
			line += '\n\t[TO id] ' + data.CHATtoIdentifier[i]
			line += '\n\t[TO name] ' + data.CHATtoName[i]
			line += '\n\t[Attachment name] ' + data.CHATattachmentFileName[i]
			line += '\n\t[Attachment path] ' + data.CHATattachmentFilePath[i]
			line += '\n\t[Attachment size] ' + data.CHATattachmentFileSize[i]
			line += '\n\t[Attachment sha1] ' + data.CHATattachmentFileHash[i]
			self.dFileHandle.write(line)

	def writeDebugCONTACT(self, data):		    	
		line = "\n*---\nTotal CONTACT (deleted)  " + str(data.CONTACTtotal)
		line += '\n*---'		
		self.dFileHandle.write(line + '\n')
		for i in range(data.CONTACTtotal):
			line = '[CONTACTid] ' + data.CONTACTid[i]  + '\n'        
			line += '[CONTACT name] ' + data.CONTACTname[i] + '\n'
			line += '[CONTACT app] ' + data.CONTACTapplication[i]  + '\n'
			line += '[CONTACT PhoneNum] ' + data.CONTACTphoneNumber[i] + '\n'
			line += '[CONTACT Identifier] ' + data.CONTACTidentifier[i] + '\n'
			self.dFileHandle.write(line)

	def writeDebugCOOKIE(self, data):		    	
		line = "\n*---\nTotal COOKIE (deleted)  " + str(data.COOKIEtotal)
		line += '\n*---'		
		self.dFileHandle.write(line + '\n')
		for i in range(data.COOKIEtotal):
			line = '[COOKIE id] ' + data.COOKIEid[i]  + '\n'        
			line += '[COOKIE name] ' + data.COOKIEname[i] + '\n'
			line += '[COOKIE value] ' + data.COOKIEvalue[i] + '\n'
			line += '[COOKIE domain] ' + data.COOKIEdomain[i] + '\n'
			line += '[COOKIE creation date] ' + data.COOKIEcreatedDate[i] + '\n'
			line += '[COOKIE expiration date] ' + data.COOKIEexpirationDate[i] + '\n'
			self.dFileHandle.write(line)
        
	def writeDebugCONTEXT(self, data):    
		line = "\n*---\nCONTEXT"
		line += '\n*---'		
		line += '\n\t[Ufed version] ' + data.CONTEXTufedVersionText
		line += '\n\t[Device Extraction start date/time] '
		line += data.CONTEXTdeviceCreationTimeText + '\n'
		line += '\n\t[Device Acquisition Start/End date/time] '
		line += data.CONTEXTdeviceExtractionStartText + ' / '
		line += data.CONTEXTdeviceExtractionEndText + '\n'
		line += '\n\t[Examiner name] '
		line += data.CONTEXTexaminerNameText + '\n'
		line += '\n\t[Bluetooth MAC] ' + data.CONTEXTdeviceBluetoothAddressText
		line += '\n\t[DeviceID] ' + data.CONTEXTdeviceIdText
		line += '\n\t[PhoneModel] ' + data.CONTEXTdevicePhoneModelText
		line += '\n\t[OS type] ' + data.CONTEXTdeviceOsTypeText
		line += '\n\t[OS version] ' + data.CONTEXTdeviceOsVersionText
		line += '\n\t[PhoneVendor] ' + data.CONTEXTdevicePhoneVendorText
		line += '\n\t[MAC] ' + data.CONTEXTdeviceMacAddressText
		line += '\n\t[ICCID] ' + data.CONTEXTdeviceIccidText
		line += '\n\t[IMSI] ' + data.CONTEXTdeviceImsiText
		line += '\n\t[IMEI] ' + data.CONTEXTdeviceImeiText
		line += '\n\t[Files]'
		for i in range (len(data.CONTEXTimagePath)):
			line += '\n\t\t[path]' + data.CONTEXTimagePath[i]
			line += '\n\t\t[size]' + data.CONTEXTimageSize[i]
			line += '\n\t\t[hash SHA256 / MD5]' + data.CONTEXTimageMetadataHashSHA[i] + ' / '
			line += data.CONTEXTimageMetadataHashMD5[i]
		self.dFileHandle.write(line)

	def writeDebugDEVICE(self, data):
		line = "\n*---\nDEVICE INFO  "
		line += '\n*---'				
		line += '\n[DEVICE ID] ' + data.DEVICEid
		line += '\n[DEVICE IMSI] ' + data.DEVICEimsi
		line += '\n[DEVICE SN] ' + data.DEVICEserialNumber
		line += '\n[DEVICE name] ' + data.DEVICEname
		line += '\n[DEVICE model] ' + data.DEVICEmodel
		line += '\n[DEVICE ICCID] ' + data.DEVICEiccid
		line += '\n[DEVICE IMEI] ' + data.DEVICEimei
		line += '\n[DEVICE OS] ' + data.DEVICEos 
		self.dFileHandle.write(line)
	
	def writeDebugEMAIL(self, data):
		line = "\n*---\nTotal EMAIL (deleted)  " + str(data.EMAILtotal)
		line += '\n*---'		
		self.dFileHandle.write(line)
		for i in range(data.EMAILtotal):
			line = '\n[EMAILid] ' + data.EMAILid[i]
			line += '\n\t[subject] ' + data.EMAILsubject[i] 
			line += '\n\t[body] ' + data.EMAILbody[i][0:30]
			line += '\n\t[appplication] ' + data.EMAILapplication[i]
			line += '\n\t[status] ' + data.EMAILstatus[i]
			line += '\n\t[date/time] ' + data.EMAILdateTime[i]
			line += '\n\t[direction] ' + data.EMAILdirection[i]
			line += '\n\t[FROM name] ' + data.EMAILfromName[i] 
			line += '\n\t[FROM email] ' + data.EMAILfromEmail[i] 
			line += '\n\t[TO name] ' + data.EMAILtoName[i] 
			line += '\n\t[TO email] ' + data.EMAILtoEmail[i] 
			line += '\n\t[CC name] ' + data.EMAILccName[i] 
			line += '\n\t[CC email] ' + data.EMAILccEmail[i] 
			line += '\n\t[BCC name] ' + data.EMAILbccName[i] 
			line += '\n\t[BCC email] ' + data.EMAILbccEmail[i] 
			line += '\n\t[ATTACHMENT file name] ' + data.EMAILattachmentFileName[i]
			line += '\n\t[ATTACHMENT file size] ' + data.EMAILattachmentFileSize[i]
			line += '\n\t[ATTACHMENT file hash] ' + data.EMAILattachmentFileHash[i]
			self.dFileHandle.write(line)        

	def writeDebugEXTRA_INFO(self, data):
		line = '\n*---\nTotal EXTRA_INFO ' + str(len(data.EXTRA_INFOdictPath))
		line += '\n*---'
		self.dFileHandle.write(line)
		for key in data.EXTRA_INFOdictPath:
			line = '\n[extraInofId] ' + key 
			line += '\n\t[path] ' + data.EXTRA_INFOdictPath[key]
			line += '\n\t[size] ' + data.EXTRA_INFOdictSize[key]
			line += '\n\t[tableName] ' + data.EXTRA_INFOdictTableName[key] 
			line += '\n\t[offset] ' + data.EXTRA_INFOdictOffset[key]
			line += '\n\t[nodeInfoId] ' + data.EXTRA_INFOdictNodeInfoId[key]
			self.dFileHandle.write(line)

	def writeDebugFILE(self, data):
		line = "\n*---\nTotal FILE " + str(len(data.FILEid))
		line += '\n*---'
		self.dFileHandle.write(line)
		for i in range(len(data.FILEid)):
			line = '\n[id]=' + data.FILEid[i]
			line += '\n\t[tag] ' + data.FILEfileTag[i]
			line += '\n\t[name] ' + data.FILEfileName[i]
			line += '\n\t[path] ' + data.FILEfilePath[i]
			line += '\n\t[size] ' + data.FILEfileSize[i]
			line += '\n\t[SHA1] ' + data.FILEsha1[i] 
			line += '\n\t[Mtime] ' + data.FILEmodified[i]  
			line += '\n\t[Atime] ' + data.FILEaccessed[i]  
			self.dFileHandle.write(line)

	def writeDebugSEARCHED_ITEM(self, data):    
		line = "\n*---\nTotal SEARCHED ITEMS  " + str(data.SEARCHED_ITEMtotal)		
		line += '\n*---'		
		self.dFileHandle.write(line)
		for i in range(data.SEARCHED_ITEMtotal):
			line = '\n[id] ' + data.SEARCHED_ITEMid[i]
			line += '\n\t[value] ' + data.SEARCHED_ITEMvalue[i]
			line += '\n\t[application] ' + data.SEARCHED_ITEMapplication[i]
			line += '\n\t[url/related url] ' + data.SEARCHED_ITEMurl[i]
			line += '\n\t[accessed] ' + data.SEARCHED_ITEMdateTime[i]
			self.dFileHandle.write(line)
	
	def writeDebugSMS(self, data):
		line = "\n*---\nTotal SMS (deleted)  " + str(data.SMStotal)
		line += '\n*---'		
		self.dFileHandle.write(line)
		for i, message in enumerate(data.SMSmessage):
			line = '\n[SMSid] ' + data.SMSid[i]
			line += '\n\t[status] ' + data.SMSstatus[i]
			line += '\n\t[application] ' + data.SMSapplication[i]
			line += '\n\t[FROM phone n.]' + data.SMSfromPhoneNumber[i]
			line += '\n\t[FROM name]' + data.SMSfromName[i]
			line += '\n\t[TO phone n.]' + data.SMStoPhoneNumber[i]
			line += '\n\t[TO name]' + data.SMStoName[i]       			
			line += '\n\t[date/time] ' + data.SMSdateTime[i]
			line += '\n\t[body] ' + message
			line += '\n\t[direction] ' + data.SMSdirection[i]
			line += '\n\t[SMSc] ' + data.SMSserviceCenterphoneNumber[i]
			self.dFileHandle.write(line)
    		    	
	def writeDebugU_ACCOUNT(self, data): 
		line = "\n*---\nTotal U_ACCOUNT " + str(data.U_ACCOUNTtotal)
		line += '\n*---'				
		self.dFileHandle.write(line)
		for i in range(len(data.U_ACCOUNTsource)):
			line = '\n\t[Source] ' + data.U_ACCOUNTsource[i] 
			line += '\n\t[Name] ' + data.U_ACCOUNTname[i] 
			line += '\n\t[User Name] ' + data.U_ACCOUNTusername[i] 
			self.dFileHandle.write(line)           


	def writeDebugWEB(self, data):    
		line = "\n*---\nTotal WEB_PAGE (deleted)  " + str(data.WEBtotal)
		line += '\n*---'		
		self.dFileHandle.write(line)
		for i in range(data.WEBtotal):
			line = '\n[id] ' + data.WEBid[i]
			line += '\n\t[app] ' + data.WEBapplication[i]
			line += '\n\t[url] ' + data.WEBurl[i]
			line += '\n\t[title] ' + data.WEBtitle[i]
			line += '\n\t[visitCount] ' + data.WEBaccessCount[i]
			line += '\n\t[lastVisited] ' + data.WEBaccessed[i]
			self.dFileHandle.write(line)

	def closeDebug(self):
		self.dFileHandle.close()

