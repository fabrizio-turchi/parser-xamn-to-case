#
#	parser_XAMNtoCASE:XAMN SAX parser from XML report to CASE-JSON-LD 
#

import xml.sax
import argparse
import os
import codecs
import XAMNtoJSON as XJ
import timeit
from time import localtime, strftime
#import logging


class XAMNparser():
    '''
    It represents all attributes and methods to process the traces extracted from XML reports by using
    the SAX parser.
    '''			
    def __init__(self, report_xml=None, json_output=None, report_type=None, 
                     base_local_path="", case_bundle=None, verbose=False):
        '''
        It initialises the main attributes for processing the XML report.
        	:param report_xml: The filename of the XML report, provided as argumnent in the command line (string).
        	:param json_output: The filename of the JSON-LD to be generated, provided as argumnent in the command line (string).
        	:param case_bundle: The container of all Objects in the JSON-LF file (list).
        	:param mode_verbose: True if the processing progress will be shown in the standard output (boolean).
        '''		

        self.xmlReport = report_xml
        self.jsonCASE = json_output
        self.reportType = report_type
#--- to be checked, is it always the same? (File/1/root/)        
        self.baseLocalPath = os.path.join(base_local_path,  'File/1/root')
        self.tic_start = timeit.default_timer()
        self.bundle = case_bundle
        self.verbose = verbose

    def show_elapsed_time(self, tic, message):
        toc = timeit.default_timer()
        C_CYAN = '\033[36m'
        C_BLACK = '\033[0m'
        elapsed_seconds = round(toc - tic, 2)
        (ss, ms) = divmod(elapsed_seconds, 1)
        elapsedMm = str(int(ss) // 60)
        elapsedSs = str(int(ss) % 60)
        elapsedMs = str(round(ms, 2))[2:]
        elapsed_time = elapsedMm + ' min. ' +  elapsedSs + ' sec. and ' + \
                    elapsedMs + ' hundredths'
        if self.verbose:
            print('\n' + C_CYAN + 'elapsed time - ' + message + ': ' + elapsed_time + '\n' + C_BLACK)

    def processXmlReport(self):
        '''
        It implements the SAX parser overriding the default ContextHandler method.
        :return: None.
        '''		
        SAXparser = xml.sax.make_parser()
        Handler = ExtractTraces(self.baseLocalPath, self.verbose)
        Handler.createOutFile(self.jsonCASE)
        SAXparser.setContentHandler(Handler)
        SAXparser.parse(self.xmlReport)

        self.show_elapsed_time(self.tic_start, 'Extraction Traces')

        if self.verbose:
            print('\n\n\nCASE JSON-LD file is being generated ...')

        tic_case_start = timeit.default_timer()

        if self.verbose:
            print(Handler.C_cyan + "owner's phone number / name: " + 
                              Handler.DEVICEphoneNumber + ' / ' + Handler.DEVICEownerName + 
                              '\n' + Handler.C_end)

        caseTrace = XJ.XAMNtoJSON(json_output=Handler.fOut, case_bundle=self.bundle)

#---    write CASE @context, version and description JSON file
        caseTrace.writeHeader()

#---    write phoneAccountFacet for the device phone number
        caseTrace.writeDeviceMobile(Handler.DEVICEid, Handler.DEVICEimsi,
            Handler.DEVICEserialNumber, Handler.DEVICEname, Handler.DEVICEmodel, 
            Handler.DEVICEimei, Handler.DEVICEiccid, Handler.DEVICEos)

        caseTrace.writePhoneOwner(Handler.DEVICEphoneNumber, Handler.DEVICEname)
        caseTrace.writePhoneAccountFromContacts(Handler.CONTACTname, 
            Handler.CONTACTphoneNumber)
        caseTrace.writeFiles(Handler.FILEid, Handler.FILEfileTag, Handler.FILEfileName,
            Handler.FILEfileSize, Handler.FILEfilePath, Handler.FILEfileLocalPath,
            Handler.FILEmodified, Handler.FILEaccessed,  Handler.FILEsha1)
        caseTrace.writeCall(Handler.CALLid, Handler.CALLapplication, Handler.CALLtimeStamp, 
            Handler.CALLdirection, Handler.CALLduration, Handler.CALLfromPartnerPhone, 
            Handler.CALLfromPartnerName, Handler.CALLfromPartnerIdentifier, 
            Handler.CALLtoPartnerPhone, Handler.CALLtoPartnerIdentifier, 
            Handler.CALLtoPartnerName)
        caseTrace.writeLocationDevice(Handler.LOCATIONid, Handler.LOCATIONlatitude, 
            Handler.LOCATIONlongitude, Handler.LOCATIONaltitude, Handler.LOCATIONtimeStamp)

        #--- Not yet identified in the XNL report
        # caseTrace.writeCell_Tower(Handler.CELL_TOWERid, Handler.CELL_TOWERmcc,
        #     Handler.CELL_TOWERmnc, Handler.CELL_TOWERlac, Handler.CELL_TOWERcid, 
        #     Handler.CELL_TOWERlongitude,Handler.CELL_TOWERlatitude,
        #     Handler.CELL_TOWERtimeStamp,  Handler.CELL_TOWERsource, 
        #     Handler.CELL_TOWERlocation, Handler.CELL_TOWERrecoveryMethod)

        #--- Not yet identified in the XNL report
        # caseTrace.writeWireless_Net(Handler.WIRELESS_NETid, Handler.WIRELESS_NETmacAddress,
        #         Handler.WIRELESS_NETchannel, Handler.WIRELESS_NETlongitude, 
        #         Handler.WIRELESS_NETlatitude, Handler.WIRELESS_NETtimeStamp,  
        #         Handler.WIRELESS_NETaccuracy, Handler.WIRELESS_NETsource, 
        #         Handler.WIRELESS_NETlocation, Handler.WIRELESS_NETrecoveryMethod)

        #--- Not yet identified in the XNL report
        # caseTrace.writeDeviceEvent(Handler.EVENT_LOCKid, Handler.EVENT_LOCKdeviceStatus, 
        #     Handler.EVENT_LOCKdateTime, Handler.EVENT_LOCKsource, 
        #     Handler.EVENT_LOCKlocation, Handler.EVENT_LOCKrecoveryMethod)

        #--- Not yet identified in the XNL report
        # caseTrace.writeAppUsage(Handler.EVENT_APP_USEid, Handler.EVENT_APP_USEapplication, 
        #          Handler.EVENT_APP_USEstartTime, Handler.EVENT_APP_USEendTime,
        #          Handler.EVENT_APP_USEsource, Handler.EVENT_APP_USElocation, 
        #          Handler.EVENT_APP_USErecoveryMethod)

        caseTrace.writeSearched_Item(Handler.SEARCHED_ITEMid, Handler.SEARCHED_ITEMvalue,
            Handler.SEARCHED_ITEMdateTime, Handler.SEARCHED_ITEMapplication, 
            Handler.SEARCHED_ITEMurl)
        caseTrace.writeSms(Handler.SMSid, Handler.SMSfromPhoneNumber, Handler.SMSfromName,
            Handler.SMSserviceCenterphoneNumber, Handler.SMStoPhoneNumber, Handler.SMStoName, 
            Handler.SMSstatus, Handler.SMSdirection, Handler.SMSdateTime, Handler.SMSmessage, 
            Handler.SMSapplication)
        
        #for i, item in enumerate(Handler.CHATdateTime):
            #if Handler.CHATbody[i].find("LEMMA") > -1:
                #print(f"item{i}, date=[{item}]")

        caseTrace.writeChat(Handler.CHATid, Handler.CHATfromIdentifier, Handler.CHATfromName,
            Handler.CHATtoIdentifier, Handler.CHATtoName, Handler.CHATdateTime, 
            Handler.CHATbody, Handler.CHATstatus, Handler.CHATapplication, 
            Handler.CHATdirection, Handler.CHATmessageType, Handler.CHATthreadId,
            Handler.CHATattachmentFileName, Handler.CHATattachmentFileSize, 
            Handler.CHATattachmentFileHash)
        caseTrace.writeCookie(Handler.COOKIEid, Handler.COOKIEname, 
            Handler.COOKIEvalue, Handler.COOKIEdomain, Handler.COOKIEcreatedDate, 
            Handler.COOKIEexpirationDate)
        caseTrace.writeEmail(Handler.EMAILid, Handler.EMAILapplication, Handler.EMAILstatus, 
            Handler.EMAILfromName, Handler.EMAILfromEmail, Handler.EMAILtoName, 
            Handler.EMAILtoEmail, Handler.EMAILccName, Handler.EMAILccEmail, 
            Handler.EMAILbccName, Handler.EMAILbccEmail, Handler.EMAILbody, 
            Handler.EMAILsubject, Handler.EMAILdateTime, Handler.EMAILattachmentFileName, 
            Handler.EMAILattachmentFileSize, Handler.EMAILattachmentFileHash)  

        #--- Not yet identified in the XNL report
        # caseTrace.writeWinTimeline(Handler.WIN_TIMELINEid, Handler.WIN_TIMELINEappName, 
        #  Handler.WIN_TIMELINEactivityType, Handler.WIN_TIMELINEtimeStamp, 
        #  Handler.WIN_TIMELINEsource, Handler.WIN_TIMELINElocation, 
        #  Handler.WIN_TIMELINErecoveryMethod)

        caseTrace.writeWebPages(Handler.WEBid, Handler.WEBapplication, 
            Handler.WEBurl, Handler.WEBtitle, Handler.WEBaccessCount,
            Handler.WEBaccessed)

        caseTrace.writeLastLine()
        self.show_elapsed_time(tic_case_start, 'Generation CASE JSON-LD file')
        Handler.fOut.close()

        return Handler


class ExtractTraces(xml.sax.ContentHandler):

    def __init__(self, baseLocalPath, verbose):
        self.fOut = ''
        self.lineXML = 0

        self.skipLine = False
        self.verbose = verbose
        self.inHit = False
        self.Observable = False
        self.C_green  = '\033[32m'
        self.C_grey  = '\033[37m'
        self.C_red = '\033[31m'
        self.C_cyan = '\033[36m'
        self.C_end = '\033[0m'

#---    DEVICE INFO
        self.DEVICEinFieldName = False
        self.DEVICEinFieldData = False

        self.DEVICEinOverview = False
        self.DEVICEinGeneral = False

        self.DEVICEos = ''
        self.DEVICEmodel = ''
        self.DEVICEname = ''
        self.DEVICEmanufacturer = ''
        self.DEVICEserialNumber = ''
        self.DEVICEid = ''        
        self.DEVICEiccid = ''
        self.DEVICEimei = ''
        self.DEVICEimsi= ''
        self.DEVICEphoneNumber = ''
        self.DEVICEownerName = ''
        self.DEVICEfieldName = ''
        self.DEVICEfieldData = ''

#---    CALL  section
        self.CALLin = False        
        self.CALLinApplication = False
        self.CALLinGroupFrom = False
        self.CALLinGroupTo = False
        self.CALLinFromPartnerPhone = False
        self.CALLinFromPartnerIdentifier = False
        self.CALLinFromPartnerName = False
        self.CALLinToPartnerPhone = False
        self.CALLinToPartnerIdentifier = False
        self.CALLinToPartnerName = False
        self.CALLinDirection = False
        self.CALLinTimeStamp = False
        self.CALLinDuration = False
        self.CALLinSource = False
        self.CALLinLocation = False
        self.CALLinRecoveryMethod = False

#---    Device phone number
#       //Artifact[@name="Android WhatsApp Accounts Information"]
#       /Hits/Hit/Fragment[@name="Phone Number"]        

        self.CALL_PHONE_NUM_DEVICEin =  False
        self.CALL_PHONE_NUM_DEVICE_VALUEin = False
        self.CALL_PHONE_NAME_DEVICE_VALUEin = False

        self.CALLapplicationText = ''
        self.CALLfromPartnerPhoneText = ''
        self.CALLfromPartnerIdentifierText = ''
        self.CALLfromPartnerNameText = ''
        self.CALLtoPartnerPhoneText = ''
        self.CALLtoPartnerIdentifierText = ''
        self.CALLtoPartnerNameText = ''
        self.CALLdirectionText = ''
        self.CALLtimeStampText = ''
        self.CALLdurationText = ''
        self.CALLappNameText = ''
        self.CALLphoneNumberDevice = 'DEVICE_PHONE_NUMBER'
        self.CALLphoneNameDevice = ''

        self.CALLtotal = 0
        self.CALLid = []
        self.CALLapplication = []
        self.CALLfromPartnerPhone = []
        self.CALLfromPartnerIdentifier = []
        self.CALLfromPartnerName = []
        self.CALLtoPartnerPhone = []
        self.CALLtoPartnerIdentifier = []
        self.CALLtoPartnerName = []
        self.CALLdirection = []        
        self.CALLtimeStamp = []
        self.CALLduration = []

#---    The CELL TOWER Artifact
        self.LOCATIONin = False
        self.LOCATIONinLatitude = False
        self.LOCATIONinLongitude = False
        self.LOCATIONinAltitude = False
        self.LOCATIONinTimeStamp = False

        self.LOCATIONlongitudeText = ''
        self.LOCATIONlatitudeText = ''
        self.LOCATIONaltitudeText = ''
        self.LOCATIONtimeStampText = ''

        self.LOCATIONtotal = 0
        self.LOCATIONid = []
        self.LOCATIONlongitude = []
        self.LOCATIONlatitude = []
        self.LOCATIONaltitude = []
        self.LOCATIONtimeStamp = []

#---    CHAT  section
        self.CHATin = False
        self.CHATinApplication = False
        self.CHATinDateTime = False
        self.CHATinCreatedDateTime = False
        self.CHATinDirection = False
        self.CHATinStatus = False
        self.CHATinMessageType = False
        self.CHATinThreadId = False        
        self.CHATinBody = False
        self.CHATinFromIdentifier = False
        self.CHATinGroupFrom = False
        self.CHATinFromName = False
        self.CHATinGroupTo = False
        self.CHATinToIdentifier = False
        self.CHATinToName = False
        self.CHATinGroupAttachment = False 
        self.CHATinAttachmentFileName = False 
        self.CHATinAttachmentFilePath = False 
        self.CHATinAttachmentFileSize = False 
        self.CHATinAttachmentFileHash = False 

        self.CHATapplicationText = ''
        self.CHATdateTimeText = ''
        self.CHATcreatedDateTimeText = ''
        self.CHATdirectionText = ''
        self.CHATstatusText = ''
        self.CHATmessageTypeText = ''
        self.CHATthreadIdText = ''
        self.CHATbodyText = ''
        self.CHATfromIdentifierText = ''
        self.CHATfromNameText = ''
        self.CHATtoIdentifierText =  ''
        self.CHATtoNameText =  ''
        self.CHATattachmentFileNameText = ''
        self.CHATattachmentFilePathText = ''
        self.CHATattachmentFileSizeText = ''
        self.CHATattachmentFileHashText = ''

        self.CHATtotal = 0
        self.CHATid = []
        self.CHATapplication = []
        self.CHATdateTime = []
        self.CHATdirection = []
        self.CHATstatus = []
        self.CHATmessageType = []
        self.CHATthreadId = []
        self.CHATbody = [] 
        self.CHATfromIdentifier = [] 
        self.CHATfromName = [] 
#---    the property TO might contain more than one participant, to distinguish them
#       the characters ### are used as separator. When the JSON-LD is generated they 
#       will be split in each single value        
        self.CHATtoIdentifier = []
        self.CHATtoName = []
        self.CHATattachmentFileName = []
        self.CHATattachmentFilePath = []
        self.CHATattachmentFileSize = []
        self.CHATattachmentFileHash = []

        self.CONTACTin = False
        self.CONTACTinName = False
        self.CONTACTinApplication = False
        self.CONTACTinPhoneNumber = False
#---    contacts related to specific applications, like Skype, Telegram etc. 
        self.CONTACTinIdentifier = False

        self.CONTACTnameText = ''
        self.CONTACTapplicationText = ''
        self.CONTACTphoneNumberText = ''
        self.CONTACTidentifierText =  ''

        self.CONTACTtotal = 0
        self.CONTACTid = []
        self.CONTACTname = []
        self.CONTACTapplication = []
        self.CONTACTphoneNumber = []
        self.CONTACTidentifier = []

#---    The Artifacts extracted are: Chrome Cookies, 
        self.COOKIE_PATTERN = ('Chrome Cookies', 'Edge Chromium Cookies', 'Firefox Cookies')
        self.COOKIEin = False        
        self.COOKIEinValue= False
        self.COOKIEinDomain = False
        self.COOKIEinCreatedDate = False
        self.COOKIEinExpirationDate = False
        self.COOKIEinName= False

        self.COOKIEvalueText = ''
        self.COOKIEnameText = ''
        self.COOKIEdomainText = ''
        self.COOKIEcreatedDateText = ''
        self.COOKIEexpirationDateText = ''

        self.COOKIEtotal = 0
        self.COOKIEid = []
        self.COOKIEvalue = []
        self.COOKIEname = []
        self.COOKIEdomain = []        
        self.COOKIEcreatedDate = []
        self.COOKIEexpirationDate = []

#---    EVENT Artifacts
        self.EVENT_LOCK_PATTERN = ('KnowledgeC Device Lock States')
        self.EVENT_LOCKin = False
        self.EVENT_LOCKinDeviceStatus= False
        self.EVENT_LOCKinDateTime= False
        self.EVENT_LOCKinSource = False
        self.EVENT_LOCKinLocation = False
        self.EVENT_LOCKinRecoveryMethod = False

        self.EVENT_LOCKdeviceStatusText = ''
        self.EVENT_LOCKdateTimeText = ''
        self.EVENT_LOCKsourceText =  ''
        self.EVENT_LOCKlocationText = ''
        self.EVENT_LOCKrecoveryMethodText = ''

        self.EVENT_LOCKtotal = 0
        self.EVENT_LOCKid = []
        self.EVENT_LOCKdeviceStatus = []
        self.EVENT_LOCKdateTime = []
        self.EVENT_LOCKsource = []
        self.EVENT_LOCKlocation = []
        self.EVENT_LOCKrecoveryMethod = []

        self.EVENT_APP_USE_PATTERN = ('KnowledgeC Application Usage')
        self.EVENT_APP_USEin = False
        self.EVENT_APP_USEinApplication= False
        self.EVENT_APP_USEinStartTime= False
        self.EVENT_APP_USEinEndTime= False
        self.EVENT_APP_USEinSource = False
        self.EVENT_APP_USEinLocation = False
        self.EVENT_APP_USEinRecoveryMethod = False

        self.EVENT_APP_USEapplicationText = ''
        self.EVENT_APP_USEstartTimeText = ''
        self.EVENT_APP_USEendTimeText =  ''
        self.EVENT_APP_USEsourceText = ''
        self.EVENT_APP_USElocationText = ''
        self.EVENT_APP_USErecoveryMethodText = ''

        self.EVENT_APP_USEtotal = 0
        self.EVENT_APP_USEid = []
        self.EVENT_APP_USEapplication = []
        self.EVENT_APP_USEstartTime = []
        self.EVENT_APP_USEendTime = []
        self.EVENT_APP_USEsource = []
        self.EVENT_APP_USElocation = []
        self.EVENT_APP_USErecoveryMethod = []

#---    EMAIL Artifacts, not yet extracted: Outlook Emails
        self.EMAILin = False
        self.EMAILinSubject = False
        self.EMAILinBody = False
        self.EMAILinApplication= False
        self.EMAILinStatus= False
        self.EMAILinDateTime = False
        self.EMAILinDirection = False
        self.EMAILinGroupFrom = False
        self.EMAILinFromName = False
        self.EMAILinFromEmail = False
        self.EMAILinGroupTo = False
        self.EMAILinToName = False
        self.EMAILinToEmail = False
        self.EMAILinGroupCc = False
        self.EMAILinCcName = False
        self.EMAILinCcEmail = False
        self.EMAILinGroupBcc = False
        self.EMAILinBccName = False
        self.EMAILinBccEmail = False
        self.EMAILinGroupAttachment = False 
        self.EMAILinAttachmentFileName = False 
        self.EMAILinAttachmentFileSize = False 
        self.EMAILinAttachmentFileHash = False 

        self.EMAILsubjectText = ''
        self.EMAILbodyText = ''
        self.EMAILapplicationText = ''
        self.EMAILstatusText = ''
        self.EMAILdateTimeText = ''
        self.EMAILdirectionText = ''
        self.EMAILfromNameText = ''
        self.EMAILfromEmailText = ''
        self.EMAILtoNameText = ''
        self.EMAILtoEmailText = ''
        self.EMAILccNameText = ''
        self.EMAILccEmailText = ''
        self.EMAILbccNameText = ''
        self.EMAILbccEmailText = ''
        self.EMAILattachmentFileNameText = ''
        self.EMAILattachmentFileSizeText = ''
        self.EMAILattachmentFileHashText = ''

        self.EMAILtotal = 0
        self.EMAILid = []
        self.EMAILsubject = []
        self.EMAILbody = []
        self.EMAILapplication = []
        self.EMAILstatus = []
        self.EMAILdateTime = []
        self.EMAILdirection = []
        self.EMAILfromName = []
        self.EMAILfromEmail = []
        self.EMAILtoName = []
        self.EMAILtoEmail = []
        self.EMAILccName = []
        self.EMAILccEmail = []
        self.EMAILbccName = []
        self.EMAILbccEmail = []
        self.EMAILattachmentFileName = []
        self.EMAILattachmentFileSize = []
        self.EMAILattachmentFileHash = []

#---    FILE  Artifacts
        self.FILE_PATTERN =('pictures_view', 'audio_view', 'video_view', 'documents_view',
                                    'databases_view', 'readable_files_view', 'files_view')
        self.FILEin = False
        self.FILEinFileName = False
        self.FILEinFileExtension = False
        self.FILEinFileSize = False
        self.FILEinFilePath = False
        self.FILEinModified = False
        self.FILEinAccessed = False
        self.FILEinSHA1 = False

        self.FILEtotal = 0
        self.FILEtagText = ''
        self.FILEfileNameText = ''
        self.FILEfileExtensionText = ''
        self.FILEbaseLocalPath = baseLocalPath
        self.FILEfileSizeText = ''
        self.FILEfilePathText = ''
        self.FILEmodifiedText = ''
        self.FILEaccessedText = ''
        self.FILEsha1Text = ''

        self.FILEid = []
        self.FILEfileName = []
        self.FILEfileTag = []
        self.FILEfileLocalPath = []
        self.FILEfileExtension = []
        self.FILEfileSize = []
        self.FILEfilePath = []
        self.FILEfileLocalPath = []
        self.FILEmodified = []
        self.FILEaccessed = []
        self.FILEsha1 = []

#---    FILE SYSTEM INFORMATION section (only for HD, USB, etc.)
        self.FILE_SYS_INFO_PATTERN = ('File System Information')
        self.FILE_SYS_INFOin = False
        self.FILE_SYS_INFOinVolumeSn = False
        self.FILE_SYS_INFOinFileSystem = False
        self.FILE_SYS_INFOinCapacity = False
        self.FILE_SYS_INFOinUnallocated = False
        self.FILE_SYS_INFOinAllocated = False
        self.FILE_SYS_INFOinOffest = False

        self.FILE_SYS_INFOvolumeSnText = ''
        self.FILE_SYS_INFOfileSystemText = ''
        self.FILE_SYS_INFOcapacityText = ''
        self.FILE_SYS_INFOunallocatedText = ''
        self.FILE_SYS_INFOallocatedText = ''
        self.FILE_SYS_INFOoffsetText = ''

        self.FILE_SYS_INFOtotal = 0
        self.FILE_SYS_INFOid = []
        self.FILE_SYS_INFOvolumeSn = []
        self.FILE_SYS_INFOfileSystem  = []
        self.FILE_SYS_INFOcapacity = []
        self.FILE_SYS_INFOunallocated = []
        self.FILE_SYS_INFOallocated = []
        self.FILE_SYS_INFOoffset = []

#---    Searched Item Artifacts   
        self.SEARCHED_ITEMin = False
        self.SEARCHED_ITEMinValue = False
        self.SEARCHED_ITEMinApplication = False
        self.SEARCHED_ITEMinUrl = False
        self.SEARCHED_ITEMinDateTime = False

        self.SEARCHED_ITEMtotal = 0
        self.SEARCHED_ITEMvalueText = ''
        self.SEARCHED_ITEMapplicationText = ''
        self.SEARCHED_ITEMurlText = ''
        self.SEARCHED_ITEMdateTimeText = ''

        self.SEARCHED_ITEMid = []
        self.SEARCHED_ITEMvalue = []
        self.SEARCHED_ITEMapplication = []
        self.SEARCHED_ITEMurl = []
        self.SEARCHED_ITEMdateTime = []

#---    SMS  Artifacts
        self.SMSin = False
        self.SMSinMessage = False
        self.SMSinApplication = False
        self.SMSinStatus = False
        self.SMSinDateTime = False
        self.SMSinDirection = False
        self.SMSinServiceCenterphoneNumber = False
        self.SMSinGroupFrom = False
        self.SMSinFromPhoneNumber = False
        self.SMSinFromName = False
        self.SMSinGroupTo = False
        self.SMSinToPhoneNumber = False
        self.SMSinToName = False

        self.SMSmessageText = ''
        self.SMSapplicationText = ''
        self.SMSstatusText = ''
        self.SMSdateTimeText = ''
        self.SMSdirectionText = ''
        self.SMSserviceCenterPhoneNumberText =  ''
        self.SMSfromPhoneNumberText = ''
        self.SMSfromNameText = ''
#---    if more than one number the string @@@ is used as separator        
        self.SMSToPhoneNumberText = ''
        self.SMSToNameText = ''

        self.SMStotal = 0
        self.SMSid = []
        self.SMSmessage = []
        self.SMSapplication = []
        self.SMSstatus = []
        self.SMSdateTime = []
        self.SMSdirection = []
        self.SMSserviceCenterphoneNumber = []
        self.SMSfromPhoneNumber = []
        self.SMSfromName = []
        self.SMStoPhoneNumber = []
        self.SMStoName = []

#---    WEB_HISTORY  Artifacts
        self.WEBin = False
        self.WEBinTitle = False
        self.WEBinUrl = False
        self.WEBinApplication = False
        self.WEBinAccessed = False 
        self.WEBinDuration = False
        self.WEBinAccessCount = False

        self.WEBtitleText = ''
        self.WEBurlText = ''
        self.WEBapplicationText = ''        
        self.WEBaccessedText = ''
        self.WEBdurationText =  ''
        self.WEBaccessCountText = ''

        self.WEBtotal = 0
        self.WEBid = []
        self.WEBtitle = []
        self.WEBurl = []
        self.WEBapplication = []
        self.WEBaccessed = []        
        self.WEBduration = []
        self.WEBaccessCount = []

#---    Wireless Network Artifact
        self.WIRELESS_NET_PATTERN = ('WiFi Locations')
        self.WIRELESS_NETin = False
        self.WIRELESS_NETinMacAaddress= False
        self.WIRELESS_NETinChannel= False
        self.WIRELESS_NETinTimeStamp = False
        self.WIRELESS_NETinLatitude = False
        self.WIRELESS_NETinLongitude = False
        self.WIRELESS_NETinAccuracy = False
        self.WIRELESS_NETinSource = False
        self.WIRELESS_NETinLocation = False
        self.WIRELESS_NETinRecoveryMethod = False

        self.WIRELESS_NETmacAddressText = ''
        self.WIRELESS_NETchannelText = ''
        self.WIRELESS_NETtimeStampText = ''
        self.WIRELESS_NETlongitudeText = ''
        self.WIRELESS_NETlatitudeText = ''
        self.WIRELESS_NETaccuracyText = ''
        self.WIRELESS_NETsourceText =  ''
        self.WIRELESS_NETlocationText = ''
        self.WIRELESS_NETrecoveryMethodText = ''

        self.WIRELESS_NETtotal = 0
        self.WIRELESS_NETid = []
        self.WIRELESS_NETmacAddress = []
        self.WIRELESS_NETchannel = []
        self.WIRELESS_NETtimeStamp = []
        self.WIRELESS_NETlongitude = []
        self.WIRELESS_NETlatitude = []
        self.WIRELESS_NETaccuracy = []
        self.WIRELESS_NETsource = []
        self.WIRELESS_NETlocation = []
        self.WIRELESS_NETrecoveryMethod = []

#---    Windows Timeline Activity Artifact, represented in CASE with the 
#       class uco-observable:EventFacet        
        self.WIN_TIMELINE_PATTERN = ('Windows Timeline Activity')
        self.WIN_TIMELINEin = False
        self.WIN_TIMELINEinAppName = False
        self.WIN_TIMELINEinActivityType = False
        self.WIN_TIMELINEinTimeStamp = False
        self.WIN_TIMELINEinSource = False
        self.WIN_TIMELINEinLocation = False
        self.WIN_TIMELINEinRecoveryMethod = False

        self.WIN_TIMELINEtotal = 0
        self.WIN_TIMELINEappNameText = ''
        self.WIN_TIMELINEactivityTypeText = ''
        self.WIN_TIMELINEtimeStampText = ''
        self.WIN_TIMELINEsourceText =  ''
        self.WIN_TIMELINElocationText = ''
        self.WIN_TIMELINErecoveryMethodText = ''

        self.WIN_TIMELINEid = []
        self.WIN_TIMELINEappName = []
        self.WIN_TIMELINEactivityType = []
        self.WIN_TIMELINEtimeStamp = []
        self.WIN_TIMELINEsource = []
        self.WIN_TIMELINElocation = []
        self.WIN_TIMELINErecoveryMethod = []        

        self.CONTEXTimagePath = []
        self.CONTEXTimageSize = []
        self.CONTEXTimageMetadataHashSHA = []
        self.CONTEXTimageMetadataHashMD5 = []


    def createOutFile(self, filename):
        self.fOut = codecs.open(filename, 'w', encoding='utf8')


    def __charactersCALL(self, ch):
        if self.CALLinApplication:
            self.CALLapplicationText += ch
        elif self.CALLinDirection:
            self.CALLdirectionText += ch
        elif self.CALLinTimeStamp:
            self.CALLtimeStampText += ch
        elif self.CALLinDuration:
            self.CALLdurationText += ch
        elif self.CALLinFromPartnerName:
            self.CALLfromPartnerNameText += ch
        elif self.CALLinFromPartnerPhone:
            self.CALLfromPartnerPhoneText += ch
        elif self.CALLinFromPartnerIdentifier:
            self.CALLfromPartnerIdentifierText += ch
        elif self.CALLinToPartnerName:
            if self.CALLtoPartnerNameText == "":
                self.CALLtoPartnerNameText += ch
            else:
                self.CALLtoPartnerNameText += "@@@" + ch
        elif self.CALLinToPartnerPhone:
            if self.CALLtoPartnerPhoneText == "":
                self.CALLtoPartnerPhoneText += ch
            else:
                self.CALLtoPartnerPhoneText += "@@@" + ch
        elif self.CALLinToPartnerIdentifier:
            if self.CALLtoPartnerIdentifierText == "":
                self.CALLtoPartnerIdentifierText += ch
            else:
                self.CALLtoPartnerIdentifierText += "@@@" + ch

    def __charactersLOCATION(self, ch):
        if self.LOCATIONinLatitude:
            self.LOCATIONlatitudeText += ch
        elif self.LOCATIONinLongitude:
            self.LOCATIONlongitudeText += ch
        elif self.LOCATIONinAltitude:
            self.LOCATIONaltitudeText += ch
        elif self.LOCATIONinTimeStamp:
            self.LOCATIONtimeStampText += ch

    def __charactersCONTACT(self, ch):
        if self.CONTACTinName:
            self.CONTACTnameText += ch
        elif self.CONTACTinApplication:
            self.CONTACTapplicationText += ch
        elif self.CONTACTinPhoneNumber:
            if self.CONTACTphoneNumberText == "":
                self.CONTACTphoneNumberText += ch
            else:
                self.CONTACTphoneNumberText += "@@@" + ch
        elif self.CONTACTinIdentifier:
            self.CONTACTidentifierText += ch

    def __charactersCOOKIE(self, ch):
        if self.COOKIEinValue:
            self.COOKIEvalueText += ch
        elif self.COOKIEinName:
            self.COOKIEnameText += ch
        elif self.COOKIEinDomain:
            self.COOKIEdomainText += ch
        elif self.COOKIEinCreatedDate:
            self.COOKIEcreatedDateText += ch
        elif self.COOKIEinExpirationDate:
            self.COOKIEexpirationDateText += ch

    def __charactersCHAT(self, ch):
        if self.CHATinApplication:
            self.CHATapplicationText += ch
        if self.CHATinDateTime:
            self.CHATdateTimeText += ch
            #print(f"CHATdateTimeText={self.CHATdateTimeText}")
        elif self.CHATinCreatedDateTime:
            self.CHATcreatedDateTimeText += ch
        elif self.CHATinDirection:
            self.CHATdirectionText += ch
        elif self.CHATinStatus:
            self.CHATstatusText += ch
        elif self.CHATinMessageType:
            self.CHATmessageTypeText += ch
        elif self.CHATinThreadId:
            self.CHATthreadIdText += ch
        elif self.CHATinBody:
            self.CHATbodyText += ch
        elif self.CHATinFromIdentifier:
            self.CHATfromIdentifierText += ch                
        elif self.CHATinFromName:
            self.CHATfromNameText += ch
        elif self.CHATinToIdentifier:
            if self.CHATtoIdentifierText == "":
                self.CHATtoIdentifierText += ch
            else:
                self.CHATtoIdentifierText += "@@@" + ch
        elif self.CHATinToName:
            if self.CHATtoNameText == "":
                self.CHATtoNameText += ch
            else:
                self.CHATtoNameText += "@@@" + ch
        elif self.CHATinAttachmentFileName:
            if self.CHATattachmentFileNameText == "":
                self.CHATattachmentFileNameText += ch
            else:
                self.CHATattachmentFileNameText += "@@@" + ch
        elif self.CHATinAttachmentFilePath:
            if self.CHATattachmentFilePathText == "":
                self.CHATattachmentFilePathText += ch
            else:
                self.CHATattachmentFilePathText += "@@@" + ch
        elif self.CHATinAttachmentFileSize:
            if self.CHATattachmentFileSizeText == "":
                self.CHATattachmentFileSizeText += ch
            else:
                self.CHATattachmentFileSizeText += "@@@" + ch
        elif self.CHATinAttachmentFileHash:
            if self.CHATattachmentFileHashText == "":
                self.CHATattachmentFileHashText += ch
            else:
                self.CHATattachmentFileHashText += "@@@" + ch

    def __charactersDEVICE_GENERAL(self, ch):        
        if self.DEVICEinFieldName:
            self.DEVICEfieldName += ch
        elif self.DEVICEinFieldData:
            if self.DEVICEfieldName == "OS":
                self.DEVICEos += ch
            if self.DEVICEfieldName == "DEVICE_NAME":
                if self.DEVICEname == "":    
                    self.DEVICEname += ch
            elif self.DEVICEfieldName == "MODEL":
                if self.DEVICEmodel == "":
                    self.DEVICEmodel += ch
            elif self.DEVICEfieldName == "MANUFACTURER":
                self.DEVICEmanufacturer += ch
            elif self.DEVICEfieldName == "SERIAL_NUMBER":
                self.DEVICEserialNumber += ch
            elif self.DEVICEfieldName == "DEVICE_ID":
                self.DEVICEid += ch
            elif self.DEVICEfieldName == "ICCID":
                self.DEVICEiccid += ch
            elif self.DEVICEfieldName == "IMSI":
                self.DEVICEimsi += ch
            elif self.DEVICEfieldName == "IMEI":
                self.DEVICEimei += ch
            elif self.DEVICEfieldName == "SUBSCRIBER_NUM":
                self.DEVICEphoneNumber += ch
            elif self.DEVICEfieldName == "OWNER_NAME":
                self.DEVICEownerName += ch

    def __charactersDEVICE_OVERVIEW(self, ch):        
        if self.DEVICEinFieldName:
            self.DEVICEfieldName += ch
        elif self.DEVICEinFieldData:
            if self.DEVICEfieldName == "OS":
                self.DEVICEos += ch

    def __charactersEVENT_LOCK(self, ch):
        if self.EVENT_LOCKinDeviceStatus:
            self.EVENT_LOCKdeviceStatusText += ch
        elif self.EVENT_LOCKinDateTime:
            self.EVENT_LOCKdateTimeText += ch
        elif self.EVENT_LOCKinSource:
            self.EVENT_LOCKsourceText += ch
        elif self.EVENT_LOCKinLocation:
            self.EVENT_LOCKlocationText += ch
        elif self.EVENT_LOCKinRecoveryMethod:
            self.EVENT_LOCKrecoveryMethodText += ch

    def __charactersEVENT_APP_USE(self, ch):
        if self.EVENT_APP_USEinStartTime:
            self.EVENT_APP_USEstartTimeText += ch
        elif self.EVENT_APP_USEinEndTime:
            self.EVENT_APP_USEendTimeText += ch
        elif self.EVENT_APP_USEinApplication:
            self.EVENT_APP_USEapplicationText += ch
        elif self.EVENT_APP_USEinLocation:
            self.EVENT_APP_USElocationText += ch
        elif self.EVENT_APP_USEinRecoveryMethod:
            self.EVENT_APP_USErecoveryMethodText += ch

    def __charactersEMAIL(self, ch):
        if self.EMAILinSubject:
            self.EMAILsubjectText += ch
        elif self.EMAILinBody:
            self.EMAILbodyText += ch
        if self.EMAILinApplication:
            self.EMAILapplicationText += ch
        elif self.EMAILinStatus:
            self.EMAILstatusText += ch
        elif self.EMAILinDateTime:
            self.EMAILdateTimeText += ch
        elif self.EMAILinDirection:
            self.EMAILdirectionText += ch
        elif self.EMAILinFromName:
            self.EMAILfromNameText += ch
        elif self.EMAILinFromEmail:
            self.EMAILfromEmailText += ch
        elif self.EMAILinToName:
            if self.EMAILtoNameText == "":
                self.EMAILtoNameText += ch
            else:
                self.EMAILtoNameText += "@@@" + ch
        elif self.EMAILinToEmail:
            if self.EMAILtoEmailText == "":
                self.EMAILtoEmailText += ch
            else:
                self.EMAILtoEmailText += "@@@" + ch
        elif self.EMAILinCcName:
            if self.EMAILccNameText == "":
                self.EMAILccNameText += ch
            else:
                self.EMAILccNameText += "@@@" + ch
        elif self.EMAILinCcEmail:
            if self.EMAILccEmailText == "":
                self.EMAILccEmailText += ch
            else:
                self.EMAILccEmailText += "@@@" + ch
        elif self.EMAILinBccEmail:
            if self.EMAILbccEmailText == "":
                self.EMAILbccEmailText += ch
            else:
                self.EMAILbccEmailText += "@@@" + ch
        elif self.EMAILinAttachmentFileName:
            if self.EMAILattachmentFileNameText == "":
                self.EMAILattachmentFileNameText += ch
            else:
                self.EMAILattachmentFileNameText += "@@@" + ch
        elif self.EMAILinAttachmentFileSize:
            if self.EMAILattachmentFileSizeText == "":
                self.EMAILattachmentFileSizeText += ch
            else:
                self.EMAILattachmentFileSizeText += "@@@" + ch
        elif self.EMAILinAttachmentFileHash:
            if self.EMAILattachmentFileHashText == "":
                self.EMAILattachmentFileHashText += ch
            else:
                self.EMAILattachmentFileHashText += "@@@" + ch


    def __charactersFILE(self, ch):
        if self.FILEinFileName:
            self.FILEfileNameText += ch
        elif self.FILEinFileExtension:
            self.FILEfileExtensionText += ch
        elif self.FILEinFileSize:
            self.FILEfileSizeText += ch
        elif self.FILEinFilePath:
            self.FILEfilePathText += ch
        elif self.FILEinModified:
            self.FILEmodifiedText += ch
        elif self.FILEinAccessed:
            self.FILEaccessedText += ch
        elif self.FILEinSHA1:
            self.FILEsha1Text += ch

    def __charactersFILE_SYS_INFO(self, ch):
        if self.FILE_SYS_INFOinVolumeSn:
            self.FILE_SYS_INFOvolumeSnText += ch
        elif self.FILE_SYS_INFOinFileSystem:
            self.FILE_SYS_INFOfileSystemText += ch
        elif self.FILE_SYS_INFOinCapacity:
            self.FILE_SYS_INFOcapacityText += ch
        elif self.FILE_SYS_INFOinUnallocated:
            self.FILE_SYS_INFOunallocatedText += ch
        elif self.FILE_SYS_INFOinAllocated:
            self.FILE_SYS_INFOallocatedText += ch
        elif self.FILE_SYS_INFOinOffest:
            self.FILE_SYS_INFOoffsetText += ch        

    def __charactersCELL_TOWER(self, ch):
        pass 

    def __charactersSEARCHED_ITEM(self, ch):
        if self.SEARCHED_ITEMinValue:
            self.SEARCHED_ITEMvalueText += ch
        elif self.SEARCHED_ITEMinApplication:
            self.SEARCHED_ITEMapplicationText += ch
        elif self.SEARCHED_ITEMinUrl:
            self.SEARCHED_ITEMurlText += ch
        elif self.SEARCHED_ITEMinDateTime:
            self.SEARCHED_ITEMdateTimeText += ch

    def __charactersSMS(self, ch):
        if self.SMSinMessage:
            self.SMSmessageText += ch
        elif self.SMSinApplication:
            self.SMSapplicationText += ch
        elif self.SMSinStatus:
            self.SMSstatusText += ch
        elif self.SMSinDateTime:
            self.SMSdateTimeText += ch
        elif self.SMSinDirection:
            self.SMSdirectionText += ch
        elif self.SMSinServiceCenterphoneNumber:
            self.SMSserviceCenterPhoneNumberText += ch
        elif self.SMSinFromPhoneNumber:
            self.SMSfromPhoneNumberText += ch
        elif self.SMSinFromName:
            self.SMSfromNameText += ch
        elif self.SMSinToPhoneNumber:
            if self.SMSToPhoneNumberText == "":
                self.SMSToPhoneNumberText += ch
            else:
                self.SMSToPhoneNumberText += "@@@" + ch

    def __charactersWEB(self, ch):
        if self.WEBinTitle:
            self.WEBtitleText += ch
        elif self.WEBinUrl:
            self.WEBurlText += ch
        elif self.WEBinApplication:
            self.WEBapplicationText += ch
        elif self.WEBinAccessed:
            self.WEBaccessedText += ch
        elif self.WEBinDuration:
            self.WEBdurationText += ch
        elif self.WEBinAccessCount:
            self.WEBaccessCountText += ch

    def __charactersWIRELESS_NET(self, ch):
        if self.WIRELESS_NETinMacAaddress:
            self.WIRELESS_NETmacAddressText += ch
        elif self.WIRELESS_NETinChannel:
            self.WIRELESS_NETchannelText += ch
        elif self.WIRELESS_NETinTimeStamp:
            self.WIRELESS_NETtimeStampText += ch
        elif self.WIRELESS_NETinLatitude:
            self.WIRELESS_NETlatitudeText += ch
        elif self.WIRELESS_NETinLongitude:
            self.WIRELESS_NETlongitudeText += ch
        elif self.WIRELESS_NETinAccuracy:
            self.WIRELESS_NETaccuracyText += ch
        elif self.WIRELESS_NETinSource:
            self.WIRELESS_NETsourceText += ch
        elif self.WIRELESS_NETinLocation:
            self.WIRELESS_NETlocationText += ch
        elif self.WIRELESS_NETinRecoveryMethod:
            self.WIRELESS_NETrecoveryMethodText += ch

    def __charactersWIN_TIMELINE(self, ch):
        if self.WIN_TIMELINEinAppName:
            self.WIN_TIMELINEappNameText += ch
        elif self.WIN_TIMELINEinActivityType:
            self.WIN_TIMELINEactivityTypeText += ch
        elif self.WIN_TIMELINEinTimeStamp:
            self.WIN_TIMELINEtimeStampText += ch
        elif self.WIN_TIMELINEinSource:
            self.WIN_TIMELINEsourceText += ch
        elif self.WIN_TIMELINEinLocation:
            self.WIN_TIMELINElocationText += ch
        elif self.WIN_TIMELINEinRecoveryMethod:
            self.WIN_TIMELINErecoveryMethodText += ch

    def __startElementFragmentPHONE_NUM(self, attrFragment):
        if attrFragment == 'Phone Number': 
            self.CALL_PHONE_NUM_DEVICE_VALUEin = True 

        if attrFragment == 'WhatsApp Name': 
            self.CALL_PHONE_NAME_DEVICE_VALUEin = True 

    def __startElementPropertyCALL(self, attrProperty):
        if attrProperty == "related_application":
            self.CALLinApplication = True
        elif attrProperty == "direction":
            self.CALLinDirection = True
        elif attrProperty == "time_local":
            self.CALLinTimeStamp = True
        elif attrProperty == "duration":
            self.CALLinDuration = True                     
        elif self.CALLinGroupFrom:
            if attrProperty == 'matched_name':  
                self.CALLinFromPartnerName = True
            elif attrProperty == 'telephone':  
                self.CALLinFromPartnerPhone = True
            elif attrProperty.find('_id') > -1:  
                self.CALLinFromPartnerIdentifier = True
        elif self.CALLinGroupTo:
            if attrProperty == 'matched_name':  
                self.CALLinToPartnerName = True
            elif attrProperty == 'telephone':  
                self.CALLinToPartnerPhone = True
            elif attrProperty.find('_id') > -1:  
                self.CALLinToPartnerIdentifier = True

    def __startElementPropertyLOCATION(self, attrProperty):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view[@type="location_history_view"]/nodes/node/properties/property
        :return:  None.
        '''        
        if attrProperty == 'latitude' :
            self.LOCATIONinLatitude = True 
        elif attrProperty == 'longitude' :
            self.LOCATIONinLongitude = True 
        elif attrProperty == 'altitude' :
            self.LOCATIONinAltitude = True 
        elif attrProperty == "gps_time":
            self.LOCATIONinTimeStamp = True 

    def __startElementPropertyCHAT(self, attrProperty):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view[@type="chat_view"]/nodes/node/properties/property
        :return:  None.
        '''         
        if attrProperty == "related_application":
            self.CHATinApplication = True 
        #elif attrProperty in ("time", "time_local", "created_local", "received_local", "sent_local"):
        elif attrProperty == "time_local":
            self.CHATinDateTime = True
        elif attrProperty == "created_local":
            self.CHATinCreatedDateTime = True        
        elif attrProperty == "direction":
            self.CHATinDirection = True
        elif attrProperty == "message_status":
            self.CHATinStatus = True
        elif attrProperty == "message_type":
            self.CHATinMessageType = True
        elif attrProperty == "thread_id":
            self.CHATinThreadId = True
        elif attrProperty == "text_body":
            self.CHATinBody = True
        elif self.CHATinGroupFrom:
            if attrProperty.find('_id') > -1:
                self.CHATinFromIdentifier = True
            elif attrProperty == "matched_name":
                self.CHATinFromName = True
        elif self.CHATinGroupTo:
            if attrProperty.find('_id') > -1:
                self.CHATinToIdentifier = True
            elif attrProperty == "matched_name":
                self.CHATinToName = True
        elif self.CHATinGroupAttachment:
            if attrProperty in ("file_name", "related_url"):
                self.CHATinAttachmentFileName = True 
            elif attrProperty == "file_path":
                self.CHATinAttachmentFilePath = True
            elif attrProperty == "size":
                self.CHATinAttachmentFileSize = True
            elif attrProperty == "sha1":
                self.CHATinAttachmentFileHash = True

    def __startElementPropertyCONTACT(self, attrType):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view[@type="contacts_view"]/nodes/node/properties/property
        :return:  None.
        '''         
        if attrType == 'name':  
            self.CONTACTinName = True 
        elif attrType == 'related_application':  
            self.CONTACTinApplication = True 
        elif attrType in ('cell_nr', 'telephone', 'work_nr'):  
            self.CONTACTinPhoneNumber = True 
        elif attrType.find('_id') > -1:  
            self.CONTACTinIdentifier = True

    def __startElementPropertyCOOKIE(self, attrType):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view[@type="web_cookies_view"]/nodes/node/properties/property
        :return:  None.
        '''        
        if attrType == 'cookie_value':
            self.COOKIEinValue = True
        elif attrType == "domain":
            self.COOKIEinDomain = True
        elif attrType == "created":
            self.COOKIEinCreatedDate = True
        elif attrType == "expires":
            self.COOKIEinExpirationDate = True
        elif attrType == "name_generic":
            self.COOKIEinName = True

    def __startElementPropertyDEVICE(self, attrType):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view[@type="device_overview_view"]/nodes/node/properties/property
        :return:  None.
        '''        
        if attrType == 'field_name':
            self.DEVICEinFieldName = True
        elif attrType == 'field_data':
            self.DEVICEinFieldData = True


    def __startElementFragmentEVENT_APP_USE(self, attrFragment):
        if attrFragment == 'State':  
            self.EVENT_LOCKinDeviceStatus = True 
        elif attrFragment.find('Start Date/Time') > -1:  
            self.EVENT_APP_USEinStartTime = True 
        elif attrFragment.find('End Date/Time') > -1:  
            self.EVENT_APP_USEinEndTime = True 
        elif attrFragment == 'Application Name':  
            self.EVENT_APP_USEinApplication = True
        elif attrFragment == 'Location':  
            self.EVENT_APP_USEinLocation = True 
        elif attrFragment.lower() == 'recovery method':  
            self.EVENT_APP_USEinRecoveryMethod = True 

    def __startElementFragmentEVENT_LOCK(self, attrFragment):
        if attrFragment == 'State':  
            self.EVENT_LOCKinDeviceStatus = True 
        elif attrFragment.find('Recorded Date/Time') > -1:  
            self.EVENT_LOCKinDateTime = True 
        elif attrFragment == 'Source':  
            self.EVENT_LOCKinSource = True
        elif attrFragment == 'Location':  
            self.EVENT_LOCKinLocation = True 
        elif attrFragment.lower() == 'recovery method':  
            self.EVENT_LOCKinRecoveryMethod = True 

    def __startElementPropertyEMAIL(self, attrType):
        if attrType == "subject":
            self.EMAILinSubject = True 
        elif attrType == "text_body":
            self.EMAILinBody = True
        elif attrType == "related_application":
            self.EMAILinApplication = True 
        elif attrType == "message_status":
            self.EMAILinStatus = True 
        elif attrType in ("received", "displayed_time", "time"):
            self.EMAILinDateTime = True 
        elif attrType == "direction":
            self.EMAILinDirection = True 
        elif self.EMAILinGroupFrom:
            if attrType in ("name", "matched_name"):
                self.EMAILinFromName = True
            elif attrType == "email":
                self.EMAILinFromEmail = True
        elif self.EMAILinGroupTo:
            if attrType.find('_name') > -1:
                self.EMAILinToName = True 
            elif attrType == "email":
                self.EMAILinToEmail = True
        elif self.EMAILinGroupCc:
            if attrType.find('_name') > -1:
                self.EMAILinCcName = True 
            elif attrType == "email":
                self.EMAILinCcEmail = True 
        elif self.EMAILinGroupBcc:
            if attrType.find('_name') > -1:
                self.EMAILinBccName = True 
            elif attrType == "email":
                self.EMAILinBccEmail = True 
        elif self.EMAILinGroupAttachment:
            if attrType == "file_name":                
                self.EMAILinAttachmentFileName = True 
            elif attrType == "size":
                self.EMAILinAttachmentFileSize = True 
            elif attrType == "sha1":
                self.EMAILinAttachmentFileHash = True         

    def __startElementPropertyFILE(self, attrType):
        if attrType == "file_name":
            self.FILEinFileName = True
        elif attrType == 'file_format':
            self.FILEinFileExtension = True
        elif attrType == "size":
            self.FILEinFileSize = True
        elif attrType == "file_path":
            self.FILEinFilePath = True
        elif attrType == "modified":
            self.FILEinModified = True
        elif attrType == "accessed" :
            self.FILEinAccessed = True
        elif attrType == 'sha1':
            self.FILEinSHA1 = True


    def __startElementFragmentFILE_SYS_INFO(self, attrFragment):
        if attrFragment == 'Volume Serial Number':  
            self.FILE_SYS_INFOinVolumeSn = True 
        elif attrFragment == 'File System':  
            self.FILE_SYS_INFOinFileSystem = True
        elif attrFragment == 'Total Capacity (Bytes)':  
            self.FILE_SYS_INFOinCapacity = True 
        elif attrFragment == 'Unallocated Area (Bytes)':  
            self.FILE_SYS_INFOinUnallocated = True 
        elif attrFragment == 'Allocated Area (Bytes)':  
            self.FILE_SYS_INFOinAllocated = True 
        elif attrFragment == 'Volume Offset (Bytes)':  
            self.FILE_SYS_INFOinOffest = True 

    def __startElementFragmentLOCATION(self, attrFragment):
        pass 

    def __startElementPropertySEARCHED_ITEM(self, attrType):
        if attrType in ("search_string", "page_title"):
            self.SEARCHED_ITEMinValue = True
        elif attrType == 'related_application':
            self.SEARCHED_ITEMinApplication = True
        elif attrType == 'accessed':
            self.SEARCHED_ITEMinDateTime = True
        elif attrType in ('related_url', 'url'):
            self.SEARCHED_ITEMinUrl = True

    def __startElementPropertySMS(self, attrType):
        if attrType == 'text_body':
            self.SMSinMessage = True
        elif attrType == 'related_application':
            self.SMSinApplication = True
        elif attrType == "message_status":
            self.SMSinStatus = True
        elif attrType == 'time':
            self.SMSinDateTime = True
        elif attrType == "direction":
            self.SMSinDirection = True
        elif attrType == "smsc":
            self.SMSinServiceCenterphoneNumber = True
        elif self.SMSinGroupFrom:
            if attrType == "telephone":
                self.SMSinToPhoneNumber = True 
            elif attrType == "matched_name":
                self.SMSinFromName = True 
        elif self.SMSinGroupTo:
            if attrType == "telephone":
                self.SMSinToPhoneNumber = True
            elif attrType == "matched_name":
                self.SMSinToName = True 

    def __startElementPropertyWEB(self, attrType):
        if attrType == 'page_title':
            self.WEBinTitle = True
        elif attrType == 'url':
            self.WEBinUrl = True
        elif attrType == "related_application":
            self.WEBinApplication = True        
        if attrType == 'accessed':
            self.WEBinAccessed = True
        if attrType == 'duration':
            self.WEBinDuration = True
        if attrType == 'access_count':
            self.WEBinAccessCount = True

    def __startElementFragmentWIRELESS_NET(self, attrFragment):
        if attrFragment == 'MAC Address' :  
            self.WIRELESS_NETinMacAaddress = True 
        elif attrFragment == 'Channel':  
            self.WIRELESS_NETinChannel = True 
        elif attrFragment.find('Timestamp Date/Time - UTC') > -1:  
            self.WIRELESS_NETinTimeStamp = True
        elif attrFragment == 'Latitude' :
            self.WIRELESS_NETinLatitude = True 
        elif attrFragment == 'Longitude' :
            self.WIRELESS_NETinLongitude = True 
        elif attrFragment.find('Accuracy') > - 1:
            self.WIRELESS_NETinAccuracy= True 
        elif attrFragment == 'Source':  
            self.WIRELESS_NETinSource = True 
        elif attrFragment == 'Location':  
            self.WIRELESS_NETnLocation = True 
        elif attrFragment.lower() == 'recovery method':  
            self.WIRELESS_NETinRecoveryMethod = True 

    def __startElementFragmentWIN_TIMELINE(self, attrFragment):
        if attrFragment == 'Application Name':
            self.WIN_TIMELINEinAppName = True
        if attrFragment == 'Activity Type':
            self.WIN_TIMELINEinActivityType = True
        if attrFragment.find('Start Date/Time') > -1:
            self.WIN_TIMELINEinTimeStamp = True
        if attrFragment == 'Source':
            self.WIN_TIMELINEinSource = True
        if attrFragment == 'Location':
            self.WIN_TIMELINEinLocation = True
        if attrFragment.lower() == 'recovery method':
            self.WIN_TIMELINEinRecoveryMethod = True

    def printObservable(self, oName, oCount):        
        line =  'processing traces --> ' + oName +  ' n. ' +  \
                    str(oCount) + "               " + self.C_end
        if self.verbose:
            if oCount == 1:
                print(self.C_green + '\n' + line, end='\r') 
            else:
                print(self.C_green + line, end='\r') 

#---    it captures each Element when it is opened., the order depends on their 
#       position from the beginning of the document
#            
    def startElement(self, elementName, attrs):
        '''
        It captures the opening of any XML Element, the order depends on their 
        position from the beginning of the document.
            :return:  None.
        '''          
        self.lineXML +=1
        attrType = attrs.get('type')

        if elementName == 'view':            
            self.__startElementView(attrType)            
        elif elementName == 'node':  
            self.__startElementNode()
        elif elementName == 'group':
            self.__startElementGroup(attrType)
        elif elementName == 'property':
            self.__startElementProperty(attrType)
        elif elementName == 'attachment':
            if self.EMAILin:
                self.EMAILinGroupAttachment = True
            elif self.CHATin:
                self.CHATinGroupAttachment = True

        if (not self.Observable):
            line = self.C_grey + '*\tProcessing Element <' + elementName + '> at line '
            line += str(self.lineXML) + ' ...'  + self.C_end
            if self.verbose:
                if self.skipLine:
                    print ('\n' + line , end='\r')
                    self.skipLine = False                  
                else:
                    print (line , end='\r')                  

    def __startElementView(self, attrType):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view/
            :return:  None.
        '''         
        if  attrType == "calls_view": 
            self.CALLin = True
            self.Observable = True
            self.skipLine = True   
        elif  attrType == "location_history_view":
            self.LOCATIONin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "chat_view":
            self.CHATin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "contacts_view":
            self.CONTACTin = True
            self.Observable = True
            self.skipLine = True                   
        elif  attrType == "web_cookies_view":
            self.COOKIEin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "general_view":
            self.DEVICEinGeneral = True
            #self.Observable = True
            #self.skipLine = True
        elif  attrType == "device_overview_view":
            self.DEVICEinOverview = True
            #self.Observable = True
            #self.skipLine = True
        elif  attrType == "emails_view":
            self.EMAILin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType in self.EVENT_LOCK_PATTERN:
            self.EVENT_LOCKin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType in self.EVENT_APP_USE_PATTERN:
            self.EVENT_APP_USEin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType in self.FILE_PATTERN:
            self.FILEin = True
            self.Observable = True 
            self.skipLine = True
            self.FILEtagText = attrType[0:attrType.index("_view")]

        elif  attrType in self.FILE_SYS_INFO_PATTERN:
            self.FILE_SYS_INFOin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "location_history_view":
            self.LOCATIONin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "web_searches_view":
            self.SEARCHED_ITEMin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "sms_view":
            self.SMSin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType == "web_history_view":
            self.WEBin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType in self.WIN_TIMELINE_PATTERN:
            self.WIN_TIMELINEin = True
            self.Observable = True
            self.skipLine = True
        elif  attrType in self.WIRELESS_NET_PATTERN:
            self.WIRELESS_NETin = True
            self.Observable = True
            self.skipLine = True

    def __startElementGroup(self, attrType):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view//group
            :return:  None.
        ''' 
        if self.CALLin:
            if  attrType == "group_from": 
                self.CALLinGroupFrom = True
            elif  attrType == "group_to":
                self.CALLinGroupTo = True
        elif self.CHATin:
            if  attrType == "group_from": 
                self.CHATinGroupFrom = True
            elif  attrType == "group_to":
                self.CHATinGroupTo = True
        elif self.SMSin:
            if  attrType == "group_from": 
                self.SMSinGroupFrom = True
            elif  attrType == "group_to":
                self.SMSinGroupTo = True
        elif self.EMAILin:
            if  attrType == "group_from": 
                self.EMAILinGroupFrom = True
            elif  attrType == "group_to":
                self.EMAILinGroupTo = True
            elif  attrType == "group_cc":
                self.EMAILinGroupCc = True
            elif  attrType == "group_bcc":
                self.EMAILinGroupBcc = True

    def __startElementNode(self):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view//node
            :return:  None.
        ''' 
        if self.CALLin:
            self.CALLtotal += 1
            self.printObservable('CALL', self.CALLtotal)
            self.CALLid.append(str(self.CALLtotal))
            self.CALLapplication.append('')
            self.CALLdirection.append('')
            self.CALLtimeStamp.append('')
            self.CALLduration.append('')
            self.CALLfromPartnerPhone.append('')
            self.CALLfromPartnerIdentifier.append('')
            self.CALLfromPartnerName.append('')
            self.CALLtoPartnerPhone.append('')
            self.CALLtoPartnerIdentifier.append('')
            self.CALLtoPartnerName.append('') 
        elif self.LOCATIONin:
            self.LOCATIONtotal += 1
            self.printObservable('LOCATION GPS', self.LOCATIONtotal)
            self.LOCATIONid.append(str(self.LOCATIONtotal))
            self.LOCATIONlatitude.append('')
            self.LOCATIONlongitude.append('')
            self.LOCATIONaltitude.append('')
            self.LOCATIONtimeStamp.append('')
        elif self.DEVICEinOverview:
            self.printObservable('DEVICE INFO OVERVIEW', 1)
        elif self.DEVICEinGeneral:
            self.printObservable('DEVICE INFO GENERAL', 1)
        elif self.CHATin:
            self.CHATtotal += 1
            self.printObservable('CHAT', self.CHATtotal)
            self.CHATid.append(str(self.CHATtotal))
            self.CHATapplication.append('')
            self.CHATdateTime.append('')
            self.CHATdirection.append('')
            self.CHATstatus.append('')
            self.CHATmessageType.append('')
            self.CHATthreadId.append('')
            self.CHATbody.append('')
            self.CHATfromIdentifier.append('')
            self.CHATfromName.append('')
            self.CHATtoIdentifier.append('')
            self.CHATtoName.append('')
            self.CHATattachmentFileName.append('')
            self.CHATattachmentFilePath.append('')
            self.CHATattachmentFileSize.append('')
            self.CHATattachmentFileHash.append('')
        elif self.CONTACTin:
            self.CONTACTtotal += 1
            self.printObservable('CONTACT', self.CONTACTtotal)
            self.CONTACTid.append(str(self.CONTACTtotal))
            self.CONTACTname.append('')
            self.CONTACTapplication.append('')
            self.CONTACTphoneNumber.append('')
            self.CONTACTidentifier.append('')
        elif self.COOKIEin:
            self.COOKIEtotal += 1
            self.printObservable('COOKIE', self.COOKIEtotal)
            self.COOKIEid.append(str(self.COOKIEtotal))
            self.COOKIEvalue.append('')
            self.COOKIEname.append('')            
            self.COOKIEdomain.append('')
            self.COOKIEcreatedDate.append('')
            self.COOKIEexpirationDate.append('')
        elif self.EMAILin:
            self.EMAILtotal += 1
            self.printObservable('EMAIL', self.EMAILtotal)
            self.EMAILid.append(str(self.EMAILtotal))
            self.EMAILsubject.append('')
            self.EMAILbody.append('')
            self.EMAILapplication.append("")
            self.EMAILstatus.append('')
            self.EMAILdateTime.append('')
            self.EMAILdirection.append('')
            self.EMAILfromName.append('')
            self.EMAILfromEmail.append('')
            self.EMAILtoName.append('')
            self.EMAILtoEmail.append('')
            self.EMAILccName.append('')
            self.EMAILccEmail.append('')
            self.EMAILbccName.append('')
            self.EMAILbccEmail.append('')
            self.EMAILattachmentFileName.append('')
            self.EMAILattachmentFileSize.append('')
            self.EMAILattachmentFileHash.append('')
        elif self.EVENT_LOCKin:
            self.EVENT_LOCKtotal += 1
            self.printObservable('EVENT LOCK', self.EVENT_LOCKtotal)
            self.EVENT_LOCKid.append(str(self.EVENT_LOCKtotal))
            self.EVENT_LOCKdateTime.append('')
            self.EVENT_LOCKdeviceStatus.append('')
            self.EVENT_LOCKsource.append('')
            self.EVENT_LOCKlocation.append('')
            self.EVENT_LOCKrecoveryMethod.append('')
        elif self.EVENT_APP_USEin:
            self.EVENT_APP_USEtotal += 1
            self.printObservable('EVENT APP USE', self.EVENT_APP_USEtotal)
            self.EVENT_APP_USEid.append(str(self.EVENT_APP_USEtotal))
            self.EVENT_APP_USEstartTime.append('')
            self.EVENT_APP_USEendTime.append('')
            self.EVENT_APP_USEapplication.append('')
            self.EVENT_APP_USEsource.append('')
            self.EVENT_APP_USElocation.append('')
            self.EVENT_APP_USErecoveryMethod.append('')
        elif self.FILEin:
            self.FILEtotal += 1
            self.printObservable('FILE / ' + self.FILEtagText, self.FILEtotal)
            self.FILEid.append(str(self.FILEtotal))
            self.FILEfileTag.append('')
            self.FILEfileName.append('')
            self.FILEfileLocalPath.append('')
            self.FILEfileExtension.append('')
            self.FILEfileSize.append('')
            self.FILEfilePath.append('')
            self.FILEfileLocalPath.append('')
            self.FILEmodified.append('')
            self.FILEaccessed.append('')
            self.FILEsha1.append('')
        elif self.FILE_SYS_INFOin:
            self.FILE_SYS_INFOtotal += 1
            self.printObservable('FILE SYSTEM INFO', self.FILE_SYS_INFOtotal)
            self.FILE_SYS_INFOid.append(str(self.FILE_SYS_INFOtotal))
            self.FILE_SYS_INFOvolumeSn.append('')
            self.FILE_SYS_INFOfileSystem.append('')
            self.FILE_SYS_INFOcapacity.append('')
            self.FILE_SYS_INFOunallocated.append('')
            self.FILE_SYS_INFOallocated.append('')
            self.FILE_SYS_INFOoffset.append('')
        elif self.LOCATIONin:
            self.LOCATIONtotal += 1
            self.printObservable('LOCATION DEVICE', self.LOCATIONtotal)
            self.LOCATIONid.append(str(self.LOCATIONtotal))                            
            self.LOCATIONlatitude.append('')
            self.LOCATIONlongitude.append('')
        elif self.SEARCHED_ITEMin:
            self.SEARCHED_ITEMtotal += 1
            self.printObservable('SEARCHED ITEMS', self.SEARCHED_ITEMtotal)
            self.SEARCHED_ITEMid.append(str(self.SEARCHED_ITEMtotal))
            self.SEARCHED_ITEMvalue.append('')
            self.SEARCHED_ITEMapplication.append('')
            self.SEARCHED_ITEMurl.append('')
            self.SEARCHED_ITEMdateTime.append('')
        elif self.SMSin:
            self.SMStotal += 1
            self.printObservable('SMS', self.SMStotal)
            self.SMSid.append(str(self.SMStotal))
            self.SMSmessage.append('')
            self.SMSapplication.append('')
            self.SMSstatus.append('')
            self.SMSdateTime.append('')
            self.SMSdirection.append('')
            self.SMSserviceCenterphoneNumber.append('')
            self.SMSfromPhoneNumber.append('')
            self.SMSfromName.append('')
            self.SMStoPhoneNumber.append('')
            self.SMStoName.append('')
        elif self.WEBin:
            self.WEBtotal += 1
            self.printObservable('WEB', self.WEBtotal)
            self.WEBid.append(str(self.WEBtotal))
            self.WEBtitle.append('')
            self.WEBurl.append('')
            self.WEBapplication.append('')
            self.WEBaccessed.append('')
            self.WEBduration.append('')
            self.WEBaccessCount.append('')
        elif self.WIRELESS_NETin:
            self.WIRELESS_NETtotal += 1
            self.printObservable('WIWIRELESS NETWORK', self.WIRELESS_NETtotal)
            self.WIRELESS_NETid.append(str(self.WIRELESS_NETtotal))
            self.WIRELESS_NETmacAddress.append('')
            self.WIRELESS_NETchannel.append('')
            self.WIRELESS_NETtimeStamp.append('')
            self.WIRELESS_NETlatitude.append('')
            self.WIRELESS_NETlongitude.append('')
            self.WIRELESS_NETaccuracy.append('')
            self.WIRELESS_NETsource.append('')
            self.WIRELESS_NETlocation.append('')
            self.WIRELESS_NETrecoveryMethod.append('')
        elif self.WIN_TIMELINEin:
            self.WIN_TIMELINEtotal += 1
            self.printObservable('WINDOWS TIME LINE', self.WIN_TIMELINEtotal)
            self.WIN_TIMELINEid.append(str(self.WIN_TIMELINEtotal))
            self.WIN_TIMELINEappName.append('')
            self.WIN_TIMELINEactivityType.append('')
            self.WIN_TIMELINEtimeStamp.append('')
            self.WIN_TIMELINEsource.append('')
            self.WIN_TIMELINElocation.append('')
            self.WIN_TIMELINErecoveryMethod.append('')  

    def __startElementProperty(self, attrType):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view/nodes/node/properties/property
            :return:  None.
        '''
        if self.CALLin:
            self.__startElementPropertyCALL(attrType)
        elif self.LOCATIONin:
            self.__startElementPropertyLOCATION(attrType)
        elif self.DEVICEinOverview:
            self.__startElementPropertyDEVICE(attrType)
        elif self.DEVICEinGeneral:
            self.__startElementPropertyDEVICE(attrType)
        elif self.CHATin: 
            self.__startElementPropertyCHAT(attrType)
        elif self.CONTACTin:
            self.__startElementPropertyCONTACT(attrType)
        elif self.COOKIEin:
            self.__startElementPropertyCOOKIE(attrType)
        elif self.CALL_PHONE_NUM_DEVICEin:
            self.__startElementFragmentPHONE_NUM(attrType)
        elif self.EMAILin:
            self.__startElementPropertyEMAIL(attrType)
        elif self.EVENT_LOCKin and self.inHit:
            self.__startElementFragmentEVENT_LOCK(attrType)
        elif self.EVENT_APP_USEin and self.inHit:
            self.__startElementFragmentEVENT_APP_USE(attrType)
        elif self.FILEin:
            self.__startElementPropertyFILE(attrType)
        elif self.FILE_SYS_INFOin and self.inHit:
            self.__startElementFragmentFILE_SYS_INFO(attrType)
        elif self.LOCATIONin and self.inHit:
            self.__startElementFragmentLOCATION(attrType)
        elif self.SEARCHED_ITEMin:
            self.__startElementPropertySEARCHED_ITEM(attrType)
        elif self.SMSin:
            self.__startElementPropertySMS(attrType)
        elif self.WEBin:
            self.__startElementPropertyWEB(attrType)
        elif self.WIRELESS_NETin and self.inHit:
            self.__startElementFragmentWIRELESS_NET(attrType)
        elif self.WIN_TIMELINEin and self.inHit:
            self.__startElementFragmentWIN_TIMELINE(attrType)

    def __endElementFragmentPHONE_NUM(self):        
        if self.CALL_PHONE_NUM_DEVICE_VALUEin:            
            self.CALL_PHONE_NUM_DEVICE_VALUEin = False

        if self.CALL_PHONE_NAME_DEVICE_VALUEin:            
            self.CALL_PHONE_NAME_DEVICE_VALUEin = False

    def __endElementPropertyCALL(self):
        '''
        It captures the opening of any XML Element matching with the XPath expression
        //view[@type="calls_view"]/nodes/node/properties/property
            :return:  None.
        '''        
        if self.CALLinApplication:
            self.CALLapplication[self.CALLtotal - 1] = self.CALLapplicationText
            self.CALLapplicationText = ''
            self.CALLinApplication = False
        elif self.CALLinFromPartnerPhone:
            self.CALLfromPartnerPhone[self.CALLtotal - 1] = self.CALLfromPartnerPhoneText
            self.CALLfromPartnerPhoneText = ''
            self.CALLinFromPartnerPhone = False
        elif self.CALLinFromPartnerIdentifier:
            self.CALLfromPartnerIdentifier[self.CALLtotal - 1] = \
                            self.CALLfromPartnerIdentifierText
            self.CALLfromPartnerIdentifierText = ''
            self.CALLinFromPartnerIdentifier = False
        elif self.CALLinFromPartnerName:
            self.CALLfromPartnerName[self.CALLtotal - 1] = self.CALLfromPartnerNameText
            self.CALLfromPartnerNameText = ''
            self.CALLinFromPartnerName = False
        elif self.CALLinFromPartnerPhone:
            self.CALLfromPartnerPhone[self.CALLtotal - 1] = self.CALLfromPartnerPhoneText
            self.CALLfromPartnerPhoneText = ''
            self.CALLinFromPartnerPhone = False
        elif self.CALLinToPartnerPhone:
            self.CALLtoPartnerPhone[self.CALLtotal - 1] = self.CALLtoPartnerPhoneText
            self.CALLtoPartnerPhoneText = ''
            self.CALLinToPartnerPhone = False
        elif self.CALLinToPartnerIdentifier:
            self.CALLtoPartnerIdentifier[self.CALLtotal - 1] = \
                            self.CALLtoPartnerIdentifierText
            self.CALLtoPartnerIdentifierText = ''
            self.CALLinToPartnerIdentifier = False
        elif self.CALLinToPartnerName:
            self.CALLtoPartnerName[self.CALLtotal - 1] = self.CALLtoPartnerNameText
            self.CALLtoPartnerNameText = ''
            self.CALLinToPartnerName = False
        elif self.CALLinDirection:
            self.CALLdirection[self.CALLtotal - 1] = self.CALLdirectionText
            self.CALLdirectionText = ''
            self.CALLinDirection = False
        elif self.CALLinTimeStamp:
            self.CALLtimeStamp[self.CALLtotal - 1] = self.CALLtimeStampText
            self.CALLtimeStampText = ''
            self.CALLinTimeStamp = False
        elif self.CALLinDuration:
            self.CALLduration[self.CALLtotal - 1] = self.CALLdurationText
            self.CALLdurationText = ''
            self.CALLinDuration = False

    def __endElementPropertyLOCATION(self):
        if self.LOCATIONinLatitude:
            self.LOCATIONlatitude[self.LOCATIONtotal - 1] = self.LOCATIONlatitudeText
            self.LOCATIONlatitudeText = ''
            self.LOCATIONinLatitude = False
        elif self.LOCATIONinLongitude:
            self.LOCATIONlongitude[self.LOCATIONtotal - 1] = self.LOCATIONlongitudeText
            self.LOCATIONlongitudeText = ''
            self.LOCATIONinLongitude = False
        elif self.LOCATIONinAltitude:
            self.LOCATIONaltitude[self.LOCATIONtotal - 1] = self.LOCATIONaltitudeText
            self.LOCATIONaltitudeText = ''
            self.LOCATIONinAltitude = False
        elif self.LOCATIONinTimeStamp:
            self.LOCATIONtimeStamp[self.LOCATIONtotal - 1] = self.LOCATIONtimeStampText
            self.LOCATIONtimeStampText = ''
            self.LOCATIONinTimeStamp = False

    def __endElementPropertyCHAT(self):
        if self.CHATinApplication:
            self.CHATinApplication = False
        elif self.CHATinDateTime:
            self.CHATinDateTime = False
        elif self.CHATinCreatedDateTime:
            self.CHATinCreatedDateTime = False        
        elif self.CHATinDirection:
            self.CHATinDirection = False
        elif self.CHATinStatus:
            self.CHATinStatus = False
        elif self.CHATinMessageType:
            self.CHATinMessageType = False
        elif self.CHATinThreadId:
            self.CHATinThreadId = False
        elif self.CHATinBody:
            self.CHATinBody = False
        elif self.CHATinFromIdentifier:
            self.CHATinFromIdentifier = False
        elif self.CHATinFromName:
            self.CHATinFromName = False
        elif self.CHATinToIdentifier:
            self.CHATinToIdentifier = False
        elif self.CHATinToName:
            self.CHATinToName = False
        elif self.CHATinAttachmentFileName:
            self.CHATinAttachmentFileName = False
        elif self.CHATinAttachmentFilePath:
            self.CHATinAttachmentFilePath = False
        elif self.CHATinAttachmentFileSize:
            self.CHATinAttachmentFileSize = False
        elif self.CHATinAttachmentFileHash:
            self.CHATinAttachmentFileHash = False

    def __endElementPropertySMS(self):
        if self.SMSinMessage:
            self.SMSinMessage = False 
        elif self.SMSinApplication:
            self.SMSinApplication = False
        elif self.SMSinStatus:
            self.SMSinStatus = False
        elif self.SMSinDateTime:
            self.SMSinDateTime = False
        elif self.SMSinDirection:
            self.SMSinDirection = False
        elif self.SMSinServiceCenterphoneNumber:
            self.SMSinServiceCenterphoneNumber = False
        elif self.SMSinStatus:
            self.SMSinStatus = False
        elif self.SMSinFromPhoneNumber:
            self.SMSinFromPhoneNumber = False
        elif self.SMSinFromName:
            self.SMSinFromName = False
        elif self.SMSinToPhoneNumber:
            self.SMSinToPhoneNumber = False
        elif self.SMSinToName:
            self.SMSinToName = False

    def __endElementNodeCONTACT(self):
#---    this to catch more than one phone number, cell_nr may be more
#       than one, moreover also both telephone and work_rn may be present
#        
        self.CONTACTphoneNumber[self.CONTACTtotal - 1] = self.CONTACTphoneNumberText
        self.CONTACTphoneNumberText = ''

    def __endElementNodeCHAT(self):
        self.CHATapplication[self.CHATtotal - 1] = \
                    self.CHATapplicationText
        #print(f"CHATdateTimeText={self.CHATdateTimeText}")
        if self.CHATdateTimeText == "":
            if self.CHATcreatedDateTimeText != "":
                self.CHATdateTime[self.CHATtotal - 1] = \
                    self.CHATcreatedDateTimeText
        else:
            self.CHATdateTime[self.CHATtotal - 1] = \
                self.CHATdateTimeText            
        self.CHATdirection[self.CHATtotal - 1] = \
                    self.CHATdirectionText
        self.CHATstatus[self.CHATtotal - 1] = \
                    self.CHATstatusText
        self.CHATmessageType[self.CHATtotal - 1] = \
                    self.CHATmessageTypeText
        self.CHATthreadId[self.CHATtotal - 1] = \
                    self.CHATthreadIdText
        self.CHATbody[self.CHATtotal - 1] = \
                    self.CHATbodyText
        self.CHATfromIdentifier[self.CHATtotal - 1] = \
                    self.CHATfromIdentifierText
        self.CHATfromName[self.CHATtotal - 1] = \
                    self.CHATfromNameText
        self.CHATtoIdentifier[self.CHATtotal - 1] = \
                    self.CHATtoIdentifierText
        self.CHATtoName[self.CHATtotal - 1] = \
                    self.CHATtoNameText  
        self.CHATattachmentFileName[self.CHATtotal - 1] = \
                    self.CHATattachmentFileNameText
        self.CHATattachmentFilePath[self.CHATtotal - 1] = \
                    self.CHATattachmentFilePathText
        self.CHATattachmentFileSize[self.CHATtotal - 1] = \
                    self.CHATattachmentFileSizeText
        self.CHATattachmentFileHash[self.CHATtotal - 1] = \
                    self.CHATattachmentFileHashText          

        self.CHATapplicationText = ''
        self.CHATdateTimeText = ''
        self.CHATcreatedDateTimeText = ''
        self.CHATdirectionText = ''
        self.CHATstatusText = ''
        self.CHATmessageTypeText = ''
        self.CHATthreadIdText = ''
        self.CHATbodyText = ''
        self.CHATfromIdentifierText = ''
        self.CHATinFromIdentifier = False 
        self.CHATfromNameText = ''
        self.CHATinFromName = False 
        self.CHATtoIdentifierText = ''
        self.CHATinToIdentifier = False 
        self.CHATtoNameText = ''
        self.CHATinToName = False 
        self.CHATattachmentFileNameText = ''
        self.CHATattachmentFilePathText = ''
        self.CHATattachmentFileSizeText = ''
        self.CHATattachmentFileHashText = ''

    def __endElementNodeSMS(self):
        self.SMSmessage[self.SMStotal - 1] = \
                    self.SMSmessageText
        self.SMSapplication[self.SMStotal - 1] = \
                    self.SMSapplicationText
        self.SMSstatus[self.SMStotal - 1] = \
                    self.SMSstatusText
        self.SMSdateTime[self.SMStotal - 1] = \
                    self.SMSdateTimeText
        self.SMSdirection[self.SMStotal - 1] = \
                    self.SMSdirectionText
        self.SMSserviceCenterphoneNumber[self.SMStotal - 1] = \
                    self.SMSserviceCenterPhoneNumberText
        self.SMSfromPhoneNumber[self.SMStotal - 1] = \
                    self.SMSfromPhoneNumberText
        self.SMSfromName[self.SMStotal - 1] = \
                    self.SMSfromNameText
        self.SMStoPhoneNumber[self.SMStotal - 1] = \
                    self.SMSToPhoneNumberText
        self.SMStoName[self.SMStotal - 1] = \
                    self.SMSToNameText


        self.SMSmessageText = ''
        self.SMSapplicationText = ''
        self.SMSstatusText = ''
        self.SMSdateTimeText = ''
        self.SMSdirectionText = ''
        self.SMSserviceCenterPhoneNumberText = ''
        self.SMSfromPhoneNumberText = ''
        self.SMSfromNameText = ''
        self.SMSinFromPhoneNumber = False
        self.SMSinFromName = False
        self.SMSToPhoneNumberText = ''
        self.SMSToNameText = '' 
        self.SMSinToPhoneNumber = False 
        self.SMSinToName = False    

    def __endElementNodeEMAIL(self):
        self.EMAILsubject[self.EMAILtotal - 1] = \
                    self.EMAILsubjectText
        self.EMAILbody[self.EMAILtotal - 1] = \
                    self.EMAILbodyText
        self.EMAILapplication[self.EMAILtotal - 1] = \
                    self.EMAILapplicationText
        self.EMAILstatus[self.EMAILtotal - 1] = \
                    self.EMAILstatusText
        self.EMAILdateTime[self.EMAILtotal - 1] = \
                    self.EMAILdateTimeText
        self.EMAILdirection[self.EMAILtotal - 1] = \
                    self.EMAILdirectionText
        self.EMAILfromName[self.EMAILtotal - 1] = \
                    self.EMAILfromNameText
        self.EMAILfromEmail[self.EMAILtotal - 1] = \
                    self.EMAILfromEmailText
        self.EMAILtoName[self.EMAILtotal - 1] = \
                    self.EMAILtoNameText
        self.EMAILtoEmail[self.EMAILtotal - 1] = \
                    self.EMAILtoEmailText
        self.EMAILccName[self.EMAILtotal - 1] = \
                    self.EMAILccNameText
        self.EMAILccEmail[self.EMAILtotal - 1] = \
                    self.EMAILccEmailText
        self.EMAILbccName[self.EMAILtotal - 1] = \
                    self.EMAILbccNameText
        self.EMAILbccEmail[self.EMAILtotal - 1] = \
                    self.EMAILbccEmailText
        self.EMAILattachmentFileName[self.EMAILtotal - 1] = \
                    self.EMAILattachmentFileNameText
        self.EMAILattachmentFileSize[self.EMAILtotal - 1] = \
                    self.EMAILattachmentFileSizeText
        self.EMAILattachmentFileHash[self.EMAILtotal - 1] = \
                    self.EMAILattachmentFileHashText

        self.EMAILsubjectText = ''
        self.EMAILbodyText = ''
        self.EMAILapplicationText = ''
        self.EMAILstatusText = ''
        self.EMAILdateTimeText = ''
        self.EMAILdirectionText = ''
        self.EMAILfromNameText = ''
        self.EMAILfromEmailText = ''
        self.EMAILtoNameText = ''
        self.EMAILtoEmailText = ''
        self.EMAILccNameText = ''
        self.EMAILccEmailText = ''
        self.EMAILbccNameText = ''
        self.EMAILbccEmailText = ''
        self.EMAILattachmentFileNameText = ''
        self.EMAILattachmentFileSizeText = ''
        self.EMAILattachmentFileHashText = ''       

    def __endElementPropertyCONTACT(self):
        if self.CONTACTinName:
            self.CONTACTname[self.CONTACTtotal - 1] = self.CONTACTnameText
            self.CONTACTnameText = ''
            self.CONTACTinName = False
        elif self.CONTACTinApplication:
            self.CONTACTapplication[self.CONTACTtotal - 1] = self.CONTACTapplicationText 
            self.CONTACTapplicationText = ''
            self.CONTACTinApplication = False
        elif self.CONTACTinPhoneNumber:            
            self.CONTACTinPhoneNumber = False
        elif self.CONTACTinIdentifier:
            self.CONTACTidentifier[self.CONTACTtotal - 1] = self.CONTACTidentifierText
            self.CONTACTidentifierText = ''
            self.CONTACTinIdentifier = False

    def __endElementPropertyDEVICE(self):
        if self.DEVICEinFieldName:            
            self.DEVICEinFieldName = False
        elif self.DEVICEinFieldData:
            self.DEVICEinFieldData = False
            self.DEVICEfieldName = ''
            self.DEVICEfieldData = ''

    def __endElementPropertyCOOKIE(self):
        if self.COOKIEinValue:
            self.COOKIEvalue[self.COOKIEtotal - 1] = self.COOKIEvalueText
            self.COOKIEvalueText = ''
            self.COOKIEinValue = False
        if self.COOKIEinName:
            self.COOKIEname[self.COOKIEtotal - 1] = self.COOKIEnameText
            self.COOKIEnameText = ''
            self.COOKIEinName = False
        elif self.COOKIEinDomain:
            self.COOKIEdomain[self.COOKIEtotal - 1] = self.COOKIEdomainText
            self.COOKIEdomainText = ''
            self.COOKIEinDomain = False
        elif self.COOKIEinCreatedDate:
            self.COOKIEcreatedDate[self.COOKIEtotal - 1] = self.COOKIEcreatedDateText
            self.COOKIEcreatedDateText = ''
            self.COOKIEinCreatedDate = False
        elif self.COOKIEinExpirationDate:
            self.COOKIEexpirationDate[self.COOKIEtotal - 1] = self.COOKIEexpirationDateText
            self.COOKIEexpirationDateText = ''
            self.COOKIEinExpirationDate = False

    def __endElementFragmentEVENT_APP_USE(self):
        if self.EVENT_APP_USEinStartTime:
            self.EVENT_APP_USEstartTime[self.EVENT_APP_USEtotal - 1] = \
                            self.EVENT_APP_USEstartTimeText
            self.EVENT_APP_USEstartTimeText = ''
            self.EVENT_APP_USEinStartTime = False

        if self.EVENT_APP_USEinEndTime:
            self.EVENT_APP_USEendTime[self.EVENT_APP_USEtotal - 1] = \
                            self.EVENT_APP_USEendTimeText
            self.EVENT_APP_USEendTimeText = ''
            self.EVENT_APP_USEinEndTime = False

        if self.EVENT_APP_USEinApplication:
            self.EVENT_APP_USEapplication[self.EVENT_APP_USEtotal - 1] = \
                            self.EVENT_APP_USEapplicationText
            self.EVENT_APP_USEapplicationText = ''
            self.EVENT_APP_USEinApplication = False

        if self.EVENT_APP_USEinSource:
            self.EVENT_APP_USEsource[self.EVENT_APP_USEtotal - 1] = \
                            self.EVENT_APP_USEsourceText
            self.EVENT_APP_USEsourceText = ''
            self.EVENT_APP_USEinSource = False

        if self.EVENT_APP_USEinLocation:
            self.EVENT_APP_USElocation[self.EVENT_APP_USEtotal - 1] = \
                            self.EVENT_APP_USElocationText
            self.EVENT_APP_USElocationText = ''
            self.EVENT_APP_USEinLocation = False

        if self.EVENT_APP_USEinRecoveryMethod:
            self.EVENT_APP_USErecoveryMethod[self.EVENT_APP_USEtotal - 1] = \
                            self.EVENT_APP_USErecoveryMethodText
            self.EVENT_APP_USErecoveryMethodText = ''
            self.EVENT_APP_USEinRecoveryMethod = False

    def __endElementFragmentEVENT_LOCK(self):
        if self.EVENT_LOCKinDateTime:
            self.EVENT_LOCKdateTime[self.EVENT_LOCKtotal - 1] = self.EVENT_LOCKdateTimeText
            self.EVENT_LOCKdateTimeText = ''
            self.EVENT_LOCKinDateTime = False
        elif self.EVENT_LOCKinDeviceStatus:
            self.EVENT_LOCKdeviceStatus[self.EVENT_LOCKtotal - 1] = \
                            self.EVENT_LOCKdeviceStatusText
            self.EVENT_LOCKdeviceStatusText = ''
            self.EVENT_LOCKinDeviceStatus = False
        elif self.EVENT_LOCKinSource:
            self.EVENT_LOCKsource[self.EVENT_LOCKtotal - 1] = self.EVENT_LOCKsourceText
            self.EVENT_LOCKsourceText = ''
            self.EVENT_LOCKinSource = False
        elif self.EVENT_LOCKinLocation:
            self.EVENT_LOCKlocation[self.EVENT_LOCKtotal - 1] = self.EVENT_LOCKlocationText
            self.EVENT_LOCKlocationText = ''
            self.EVENT_LOCKinLocation = False
        elif self.EVENT_LOCKinRecoveryMethod:
            self.EVENT_LOCKrecoveryMethod[self.EVENT_LOCKtotal - 1] = \
                            self.EVENT_LOCKrecoveryMethodText
            self.EVENT_LOCKrecoveryMethodText = ''
            self.EVENT_LOCKinRecoveryMethod = False

    def __endElementPropertyEMAIL(self):
        if self.EMAILinSubject:
            self.EMAILinSubject = False
        elif self.EMAILinBody:
            self.EMAILinBody = False
        elif self.EMAILinApplication:
            self.EMAILinApplication = False
        elif self.EMAILinStatus:
            self.EMAILinStatus = False
        elif self.EMAILinDateTime:
            self.EMAILinDateTime = False
        elif self.EMAILinDirection:
            self.EMAILinDirection = False
        elif self.EMAILinFromName:
            self.EMAILinFromName = False
        elif self.EMAILinFromEmail:
            self.EMAILinFromEmail = False
        elif self.EMAILinToName:
            self.EMAILinToName = False
        elif self.EMAILinToEmail:
            self.EMAILinToEmail = False
        elif self.EMAILinCcName:
            self.EMAILinCcName = False
        elif self.EMAILinCcEmail:
            self.EMAILinCcEmail = False
        elif self.EMAILinBccName:
            self.EMAILinBccName = False
        elif self.EMAILinBccEmail:
            self.EMAILinBccEmail = False
        elif self.EMAILinAttachmentFileName:
            self.EMAILinAttachmentFileName = False
        elif self.EMAILinAttachmentFileSize:
            self.EMAILinAttachmentFileSize = False
        elif self.EMAILinAttachmentFileHash:
            self.EMAILinAttachmentFileHash = False

    def __endElementPropertyFILE(self):
        if self.FILEinFileName:
            self.FILEfileName[self.FILEtotal - 1] =  self.FILEfileNameText
            self.FILEfileTag[self.FILEtotal - 1] =  self.FILEtagText
            self.FILEfileNameText = ''
            self.FILEinFileName = False 
        elif self.FILEinFileExtension:
            self.FILEfileExtension[self.FILEtotal - 1] =  self.FILEfileExtensionText
            self.FILEfileExtensionText = ''
            self.FILEinFileExtension = False 
        elif self.FILEinFileSize:
            self.FILEfileSize[self.FILEtotal - 1] =  self.FILEfileSizeText
            self.FILEfileSizeText = ''
            self.FILEinFileSize = False 
        elif self.FILEinFilePath:
            self.FILEfilePath[self.FILEtotal - 1] =  self.FILEfilePathText            
            self.FILEfileLocalPath[self.FILEtotal - 1] =  self.FILEbaseLocalPath + \
                            self.FILEfilePathText
            self.FILEfilePathText = ''
            self.FILEinFilePath = False 
        elif self.FILEinModified:
            self.FILEmodified[self.FILEtotal - 1] =  self.FILEmodifiedText
            self.FILEmodifiedText = ''
            self.FILEinModified = False 
        elif self.FILEinAccessed:
            self.FILEaccessed[self.FILEtotal - 1] =  self.FILEaccessedText
            self.FILEaccessedText = ''
            self.FILEinAccessed = False 
        elif self.FILEinSHA1:
            self.FILEsha1[self.FILEtotal - 1] =  self.FILEsha1Text
            self.FILEsha1Text = ''
            self.FILEinSHA1 = False 

    def __endElementFragmentFILE_SYS_INFO(self):
        if self.FILE_SYS_INFOinVolumeSn:
            self.FILE_SYS_INFOvolumeSn[self.FILE_SYS_INFOtotal - 1] = self.FILE_SYS_INFOvolumeSnText
            self.FILE_SYS_INFOvolumeSnText = ''
            self.FILE_SYS_INFOinVolumeSn = False
        elif self.FILE_SYS_INFOinFileSystem:
            self.FILE_SYS_INFOfileSystem[self.FILE_SYS_INFOtotal - 1] = self.FILE_SYS_INFOfileSystemText
            self.FILE_SYS_INFOfileSystemText = ''
            self.FILE_SYS_INFOinFileSystem = False
        elif self.FILE_SYS_INFOinCapacity:
            self.FILE_SYS_INFOcapacity[self.FILE_SYS_INFOtotal - 1] = self.FILE_SYS_INFOcapacityText
            self.FILE_SYS_INFOcapacityText = ''
            self.FILE_SYS_INFOinCapacity = False
        elif self.FILE_SYS_INFOinUnallocated:
            self.FILE_SYS_INFOunallocated[self.FILE_SYS_INFOtotal - 1] = self.FILE_SYS_INFOunallocatedText
            self.FILE_SYS_INFOunallocatedText = ''
            self.FILE_SYS_INFOinUnallocated = False
        elif self.FILE_SYS_INFOinAllocated:
            self.FILE_SYS_INFOallocated[self.FILE_SYS_INFOtotal - 1] = self.FILE_SYS_INFOallocatedText
            self.FILE_SYS_INFOallocatedText = ''
            self.FILE_SYS_INFOinAllocated = False
        elif self.FILE_SYS_INFOinOffest:
            self.FILE_SYS_INFOoffset[self.FILE_SYS_INFOtotal - 1] = self.FILE_SYS_INFOoffsetText
            self.FILE_SYS_INFOoffsetText = ''
            self.FILE_SYS_INFOinOffest = False

    def __endElementFragmentLOCATION(self):
        pass 

    def __endElementPropertySEARCHED_ITEM(self):
        if self.SEARCHED_ITEMinValue:            
            self.SEARCHED_ITEMvalue[self.SEARCHED_ITEMtotal - 1] = \
                            self.SEARCHED_ITEMvalueText            
            self.SEARCHED_ITEMvalueText = ''
            self.SEARCHED_ITEMinValue = False
        elif self.SEARCHED_ITEMinApplication:
            self.SEARCHED_ITEMapplication[self.SEARCHED_ITEMtotal - 1] = \
                            self.SEARCHED_ITEMapplicationText
            self.SEARCHED_ITEMapplicationText = ''
            self.SEARCHED_ITEMinApplication = False
        elif self.SEARCHED_ITEMinUrl:
            self.SEARCHED_ITEMurl[self.SEARCHED_ITEMtotal - 1] = \
                            self.SEARCHED_ITEMurlText
            self.SEARCHED_ITEMurlText = ''
            self.SEARCHED_ITEMinUrl = False
        elif self.SEARCHED_ITEMinDateTime:
            self.SEARCHED_ITEMdateTime[self.SEARCHED_ITEMtotal - 1] = \
                            self.SEARCHED_ITEMdateTimeText
            self.SEARCHED_ITEMdateTimeText = ''
            self.SEARCHED_ITEMinDateTime = False

    def __endElementFragmentSMS(self):
        if self.SMSinPartner:
            self.SMSinPartner = False

        if self.SMSinRecipient:
            #self.SMSrecipient[self.SMStotal - 1] = self.SMSrecipientText                    
            self.SMSrecipientText = ''            
            self.SMSinRecipient = False

        if self.SMSinSender:
            #self.SMSsender[self.SMStotal - 1] = self.SMSsenderText
            self.SMSsenderText = ''
            self.SMSinSender = False

        if self.SMSinReceivedDateTime:
            #self.SMSreceivedDateTime[self.SMStotal - 1] = self.SMSreceivedDateTimeText        
            self.SMSreceivedDateTimeText = ''
            self.SMSinReceivedDateTime = False

        if self.SMSinSentDateTime:
            #self.SMSsentDateTime[self.SMStotal - 1] = self.SMSsentDateTimeText        
            self.SMSsentDateTimeText = ''
            self.SMSinSentDateTime = False

        if self.SMSinMessage:
            self.SMSmessage[self.SMStotal - 1] = self.SMSmessageText                    
            self.SMSmessageText = ''
            self.SMSinMessage = False

        if self.SMSinDirection:
            self.SMSinDirection = False

        if self.SMSinSource:
            #self.SMSsource[self.SMStotal - 1] = self.SMSsourceText
            self.SMSsourceText = ''
            self.SMSinSource = False

        if self.SMSinLocation:
            #self.SMSlocation[self.SMStotal - 1] = self.SMSlocationText
            self.SMSlocationText = ''
            self.SMSinLocation = False

        if self.SMSinRecoveryMethod:
            #self.SMSrecoveryMethod[self.SMStotal - 1] = self.SMSrecoveryMethodText
            self.SMSrecoveryMethodText = ''
            self.SMSinRecoveryMethod = False            

#---    Android SMS includes the Partner element.
#       iOS iMessage/SMS/MMS includes the Sender and Recipient(s) elements and the
#       Incoming from Outgoing messages are identified by Received Date/time or Sent Date/Time                 only the Message Sent Date/Time is set, so in case
#       elements           
#            
            if self.SMSpartnerText != '':
                if self.SMSdirectionText.lower() == 'outgoing':                    
                    pass
                    #self.SMSsender[self.SMStotal - 1] = self.CALLphoneNumberDevice
                    #self.SMSrecipient[self.SMStotal - 1] = self.SMSpartnerText                    
                else:
                    pass
                    #self.SMSsender[self.SMStotal - 1] = self.SMSpartnerText
                    #self.SMSrecipient[self.SMStotal - 1] = self.CALLphoneNumberDevice                    
                self.SMSpartnerText = ''
                self.SMSdirectionText = ''                

    def __endElementPropertyWEB(self):
        if self.WEBinTitle:
            self.WEBtitle[self.WEBtotal - 1] = self.WEBtitleText        
            self.WEBtitleText = ''
            self.WEBinTitle = False
        elif self.WEBinUrl:
            self.WEBurl[self.WEBtotal - 1] = self.WEBurlText        
            self.WEBurlText = ''
            self.WEBinUrl = False
        elif self.WEBinApplication:
            self.WEBapplication[self.WEBtotal - 1] = self.WEBapplicationText
            self.WEBapplicationText = ''
            self.WEBinApplication = False
        elif self.WEBinAccessed:
            self.WEBaccessed[self.WEBtotal - 1] = self.WEBaccessedText
            self.WEBaccessedText = ''
            self.WEBinAccessed = False
        elif self.WEBinDuration:
            self.WEBduration[self.WEBtotal - 1] = self.WEBdurationText
            self.WEBdurationText = ''
            self.WEBinDuration = False
        elif self.WEBinAccessCount:
            self.WEBaccessCount[self.WEBtotal - 1] = self.WEBaccessCountText
            self.WEBaccessCountText = ''
            self.WEBinAccessCount = False

    def __endElementFragmentWIRELESS_NET(self):
        if self.WIRELESS_NETinMacAaddress:
            self.WIRELESS_NETmacAddress[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETmacAddressText
            self.WIRELESS_NETmacAddressText = ''
            self.WIRELESS_NETinMacAaddress = False
        elif self.WIRELESS_NETinChannel:
            self.WIRELESS_NETchannel[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETchannelText
            self.WIRELESS_NETchannelText = ''
            self.WIRELESS_NETinChannel = False
        elif self.WIRELESS_NETinTimeStamp:
            self.WIRELESS_NETtimeStamp[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETtimeStampText
            self.WIRELESS_NETtimeStampText = ''
            self.WIRELESS_NETinTimeStamp = False
        elif self.WIRELESS_NETinLatitude:
            self.WIRELESS_NETlatitude[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETlatitudeText
            self.WIRELESS_NETlatitudeText = ''
            self.WIRELESS_NETinLatitude = False
        elif self.WIRELESS_NETinLongitude:
            self.WIRELESS_NETlongitude[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETlongitudeText
            self.WIRELESS_NETlongitudeText = ''
            self.WIRELESS_NETinLongitude = False
        elif self.WIRELESS_NETinAccuracy:
            self.WIRELESS_NETaccuracy[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETaccuracyText
            self.WIRELESS_NETaccuracyText = ''
            self.WIRELESS_NETinAccuracy = False
        elif self.WIRELESS_NETinSource:
            self.WIRELESS_NETsource[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETsourceText
            self.WIRELESS_NETsourceText = ''
            self.WIRELESS_NETinSource = False
        elif self.WIRELESS_NETinLocation:
            self.WIRELESS_NETlocation[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETlocationText
            self.WIRELESS_NETlocationText = ''
            self.WIRELESS_NETinLocation = False
        elif self.WIRELESS_NETinRecoveryMethod:
            self.WIRELESS_NETrecoveryMethod[self.WIRELESS_NETtotal - 1] = \
                            self.WIRELESS_NETrecoveryMethodText
            self.WIRELESS_NETrecoveryMethodText = ''
            self.WIRELESS_NETinRecoveryMethod = False

    def __endElementFragmentWIN_TIMELINE(self):
        if self.WIN_TIMELINEinAppName:
            self.WIN_TIMELINEappName[self.WIN_TIMELINEtotal - 1] = self.WIN_TIMELINEappNameText
            self.WIN_TIMELINEappNameText = ''
            self.WIN_TIMELINEinAppName = False

        if self.WIN_TIMELINEinActivityType:
            self.WIN_TIMELINEactivityType[self.WIN_TIMELINEtotal - 1] = self.WIN_TIMELINEactivityTypeText
            self.WIN_TIMELINEactivityTypeText = ''
            self.WIN_TIMELINEinActivityType = False

        if self.WIN_TIMELINEinTimeStamp:
            self.WIN_TIMELINEtimeStamp[self.WIN_TIMELINEtotal - 1] = self.WIN_TIMELINEtimeStampText
            self.WIN_TIMELINEtimeStampText = ''
            self.WIN_TIMELINEinTimeStamp = False

        if self.WIN_TIMELINEinSource:
            self.WIN_TIMELINEsource[self.WIN_TIMELINEtotal - 1] = self.WIN_TIMELINEsourceText
            self.WIN_TIMELINEsourceText = ''
            self.WIN_TIMELINEinSource = False

        if self.WIN_TIMELINEinLocation:
            self.WIN_TIMELINElocation[self.WIN_TIMELINEtotal - 1] = self.WIN_TIMELINElocationText
            self.WIN_TIMELINElocationText = ''
            self.WIN_TIMELINEinLocation = False

        if self.WIN_TIMELINEinRecoveryMethod:
            self.WIN_TIMELINErecoveryMethod[self.WIN_TIMELINEtotal - 1] = self.WIN_TIMELINErecoveryMethodText
            self.WIN_TIMELINErecoveryMethodText = ''
            self.WIN_TIMELINEinRecoveryMethod = False

    def __endElementNodesDEVICE_OVERVIEW(self):
        self.DEVICEos = ''

#    It captures each Element when it is closed
    def endElement(self, name):
        if name == 'property':
            if self.CALLin:
                self.__endElementPropertyCALL()
            elif self.LOCATIONin:
                self.__endElementPropertyLOCATION()
            elif self.CHATin:
                self.__endElementPropertyCHAT()
            elif self.CONTACTin:
                self.__endElementPropertyCONTACT()
            elif self.COOKIEin:
                self.__endElementPropertyCOOKIE()
            elif self.DEVICEinOverview or self.DEVICEinGeneral:
                self.__endElementPropertyDEVICE()            
            elif self.EMAILin:
                self.__endElementPropertyEMAIL()
            elif self.EVENT_LOCKin:
                self.__endElementFragmentEVENT_LOCK()
            elif self.EVENT_APP_USEin:
                self.__endElementFragmentEVENT_APP_USE()
            elif self.FILEin:
                self.__endElementPropertyFILE()
            elif self.FILE_SYS_INFOin:
                self.__endElementFragmentFILE_SYS_INFO()
            elif self.LOCATIONin:
                self.__endElementFragmentLOCATION()
            elif self.SEARCHED_ITEMin:
                self.__endElementPropertySEARCHED_ITEM()
            elif self.SMSin:
                self.__endElementPropertySMS()
            elif self.WEBin:
                self.__endElementPropertyWEB()
            elif self.WIN_TIMELINEin:
                self.__endElementFragmentWIN_TIMELINE()            
            elif self.WIRELESS_NETin:
                self.__endElementFragmentWIRELESS_NET()
        elif name == "group":
            if self.CALLinGroupFrom:
                self.CALLinGroupFrom = False
            elif self.CALLinGroupTo:
                self.CALLinGroupTo = False
            elif self.CHATinGroupFrom:
                self.CHATinGroupFrom = False
            elif self.CHATinGroupTo:
                self.CHATinGroupTo = False
            elif self.SMSinGroupFrom:
                self.SMSinGroupFrom = False
            elif self.SMSinGroupTo:
                self.SMSinGroupTo = False
            elif self.EMAILinGroupFrom:
                self.EMAILinGroupFrom = False
            elif self.EMAILinGroupTo:
                self.EMAILinGroupTo = False
            elif self.EMAILinGroupCc:
                self.EMAILinGroupCc = False
            elif self.EMAILinGroupBcc:
                self.EMAILinGroupBcc = False
        elif name == "attachment":
            if self.EMAILin:
                self.EMAILinGroupAttachment = False
            if self.CHATin:
                self.CHATinGroupAttachment = False

        elif name == "node":
            if self.CONTACTin:
                self.__endElementNodeCONTACT()
            elif self.CHATin:
                self.__endElementNodeCHAT()
            elif self.SMSin:
                self.__endElementNodeSMS()
            elif self.EMAILin:
                self.__endElementNodeEMAIL()
        elif name == 'nodes':
            if self.CALLin:
                self.CALLin = False
                self.Observable = False
            elif self.CALL_PHONE_NUM_DEVICEin:
                self.CALL_PHONE_NUM_DEVICEin = False
                self.Observable = False
            elif self.LOCATIONin:
                self.LOCATIONin = False
                self.Observable = False
            elif self.CHATin:
                self.CHATin = False
                self.Observable = False
            elif self.CONTACTin:
                self.CONTACTin = False
                self.Observable = False
            elif self.COOKIEin:
                self.COOKIEin = False
                self.Observable = False
            elif self.DEVICEinGeneral:                
                self.DEVICEinGeneral = False
                self.Observable = False
            elif self.DEVICEinOverview:                
                self.__endElementNodesDEVICE_OVERVIEW()
                self.DEVICEinOverview = False
                self.Observable = False
            elif self.EVENT_LOCKin:
                self.EVENT_LOCKin = False
                self.Observable = False
            elif self.EVENT_APP_USEin:
                self.EVENT_APP_USEin = False
                self.Observable = False
            elif self.EMAILin:
                self.EMAILin = False
                self.Observable = False
            elif self.FILEin:
                self.FILEin = False
                self.Observable = False 
                self.FILEtagText = ""
            elif self.FILE_SYS_INFOin:
                self.FILE_SYS_INFOin = False
                self.Observable = False
            elif self.LOCATIONin:
                self.LOCATIONin = False
                self.Observable = False
            elif self.SEARCHED_ITEMin:
                self.SEARCHED_ITEMin = False
                self.Observable = False 
            elif self.SMSin:
                self.SMSin = False
                self.Observable = False 
            elif self.WEBin:
                self.WEBin = False
                self.Observable = False
            elif self.WIRELESS_NETin:
                self.WIRELESS_NETin = False
                self.Observable = False
            elif self.WIN_TIMELINEin:
                self.WIN_TIMELINEin = False
                self.Observable = False               

#---    it captures the value/character inside the Text Elements
    def characters(self, ch):        
        if self.CALLin:
            self.__charactersCALL(ch)           
        elif self.LOCATIONin:
            self.__charactersLOCATION(ch)
        elif self.CALL_PHONE_NUM_DEVICE_VALUEin:            
            if self.CALLphoneNumberDevice == 'DEVICE_PHONE_NUMBER':
                self.CALLphoneNumberDevice = ch
        elif self.CALL_PHONE_NAME_DEVICE_VALUEin:            
            self.CALLphoneNameDevice += ch
        elif self.CHATin:
            self.__charactersCHAT(ch)
        elif self.CONTACTin:
            self.__charactersCONTACT(ch)
        elif self.COOKIEin:
            self.__charactersCOOKIE(ch)
        elif self.DEVICEinGeneral:
            self.__charactersDEVICE_GENERAL(ch)
        elif self.DEVICEinOverview:
            self.__charactersDEVICE_OVERVIEW(ch)
        elif self.EMAILin:
            self.__charactersEMAIL(ch)
        elif self.EVENT_LOCKin:
            self.__charactersEVENT_LOCK(ch)
        elif self.EVENT_APP_USEin:
            self.__charactersEVENT_APP_USE(ch)
        elif self.FILEin:
            self.__charactersFILE(ch)
        elif self.FILE_SYS_INFOin:
            self.__charactersFILE_SYS_INFO(ch)
        elif self.LOCATIONin:
            self.__charactersLOCATION(ch)
        elif self.SEARCHED_ITEMin:
            self.__charactersSEARCHED_ITEM(ch)
        elif self.SMSin:
            self.__charactersSMS(ch)
        elif self.WEBin:
            self.__charactersWEB(ch)
        elif self.WIRELESS_NETin:
            self.__charactersWIRELESS_NET(ch)
        elif self.WIN_TIMELINEin:
            self.__charactersWIN_TIMELINE(ch)            

if __name__ == '__main__':

    C_CYAN = '\033[36m'
    C_BLACK = '\033[0m'

    parserArgs = argparse.ArgumentParser(description='Parser to convert XML Report from XAMN into CASE standard.')

#---    report XML exported by AXIOM to be converted/parsed into CASE
    parserArgs.add_argument('-r', '--report', dest='inFileXML', required=True, 
                                help='The XAMN XML report from which to extract digital traces and convert them into CASE')


#---    Type of device mobile or disk (HD, USB, SD, etc)
    parserArgs.add_argument('-o', '--output', dest='outCASE_JSON', required=True, help='File CASE-JSON-LD of output')

    parserArgs.add_argument('-d', '--debug', dest='outputDebug', required=False, help='File for writing debug')

    args = parserArgs.parse_args()

    print('*--- Input paramaters start \n')
    print('\tFile XML:\t\t' + args.inFileXML)

    head, tail = os.path.split(args.outCASE_JSON)
    print('\tFile Output:\t\t' + args.outCASE_JSON)

    if args.outputDebug is None:
        pass
    else:
        print('\tFile Debug:\t\t' + args.outputDebug)

    print('\n*--- Input paramaters end')
    print('\n\n' + C_CYAN + '*** Start processing: ' + strftime("%Y-%m-%d %H:%M:%S", localtime()) + C_BLACK + '\n')

#---    baseLocalPath is for setting the fileLocalPath property of FileFacet 
#       Observable. 
    baseLocalPath = ''

    parser = XAMNparser(report_xml=args.inFileXML, json_output=args.outCASE_JSON, 
                            base_local_path=baseLocalPath, verbose=True) 

    Handler = parser.processXmlReport()    

    if args.outputDebug is None:
        pass
    else:
        import XAMNdebug
        debug = XAMNdebug.ParserDebug(args.outputDebug)
        #debug.writeDebugDEVICE(Handler)
        #debug.writeDebugCALL(Handler)
        #debug.writeDebugLOCATION(Handler)
        #debug.writeDebugSEARCHED_ITEM(Handler)
        #debug.writeDebugCHAT(Handler)
        #debug.writeDebugCONTACT(Handler)
        #debug.writeDebugCOOKIE(Handler)
        #debug.writeDebugFILE(Handler)
        #debug.writeDebugEMAIL(Handler)
        #debug.writeDebugSMS(Handler)
        #debug.writeDebugWEB(Handler)
        # debug.closeDebug() 

    parser.show_elapsed_time(parser.tic_start, 'End processing')
